<div class="chat-area">
    <div class="chat-header"
         style="background-color: white;padding: 10px;margin-bottom: 6px;border-radius: 4px;">
        <header style="border-bottom: 0px !important;"
                class="d-flex justify-content-between align-items-center border-bottom px-1 py-75">
            <div class="d-flex align-items-center">
                <h6 class="mb-0">{{ $user['name'] }} {{ $user['surname'] }}</h6>
            </div>
        </header>
    </div>
    <!-- chat card start -->
    <div class="card chat-wrapper shadow-none">
        <div class="card-content">
            <div class="card-body chat-container ps ps--active-y">
                <div class="chat-content" style="height: 395px;max-height: 395px;overflow: auto">
                    @foreach($messages as $message)
                        @if (auth()->user()->id === $message['sender_id'])
                            <div class="chat">
                                <div class="chat-body">
                                    <div class="chat-message">
                                        <p>{{ $message['message'] }}</p>
                                        <span class="chat-time">{{ date('d-m-Y h:m',strtotime($message['created_at'])) }}</span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="chat chat-left">
                                <div class="chat-body">
                                    <div class="chat-message">
                                        <p>{{ $message['message'] }}</p>
                                        <span class="chat-time">{{ date('d-m-Y h:m',strtotime($message['created_at'])) }}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="card-footer chat-footer border-top px-2 pt-1 pb-0 mb-1">
            <form class="d-flex align-items-center" onsubmit="chatMessagesSend();"
                  action="javascript:void(0);">
                <i class="bx bx-face cursor-pointer"></i>
                <i class="bx bx-paperclip ml-1 cursor-pointer"></i>
                <input type="text" class="form-control chat-message-send mx-1"
                       placeholder="{{__('messages.menu.chatSendMessagePH')}}" name="chat_message">
                <button type="submit" class="btn btn-primary glow send d-lg-flex"><i
                        class="bx bx-paper-plane"></i>
                    <span class="d-none d-lg-block ml-1">{{__('messages.menu.send')}}</span></button>
            </form>
        </div>
    </div>
    <!-- chat card ends -->
</div>
