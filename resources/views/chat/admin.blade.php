@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body chat-application" style="margin-top: 80px;">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="chat-sidebar card" style="width: auto;">
                  <span class="chat-sidebar-close">
                    <i class="bx bx-x"></i>
                  </span>
                    <div class="chat-sidebar-search">
                        <div class="d-flex align-items-center">
                            <fieldset class="form-group position-relative has-icon-left mx-75 mb-0">
                                <input type="text" class="form-control round" id="chat-search"
                                       placeholder="Search">
                                <div class="form-control-position">
                                    <i class="bx bx-search-alt text-dark"></i>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="chat-sidebar-list-wrapper pt-2 ps ps--active-y">
                        <ul class="chat-sidebar-list">
                            @foreach($users as $user)
                                <li user_id="{{ $user['id'] }}">
                                    <div class="d-flex align-items-center">
                                        <div class="chat-sidebar-name">
                                            <h6 class="mb-0">{{ $user['name'] }} {{ $user['surname'] }}</h6>
                                            <span class="text-muted">{{ $user['email'] }}</span>
                                            <span
                                                class="user-status text {{ $user['is_online'] ? 'text-success' : 'text-info'}}">
                                                {{ $user['is_online'] ? 'online' : $user['last_active_date'] }}</span>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div id="chatbox" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

            </div>
        </div>
    </div>
@endsection

@section('authenticated_js')
    <script src="{{ asset('js/app-chat.min.js') }}"></script>
    <script>
        let user_id = $('.chat-sidebar-list li:eq(0)').attr('user_id');
        $('.chat-sidebar-list li:eq(0)').addClass('active');
        if (user_id && parseInt(user_id) > 0) {
            loadContent(user_id);
        }

        $('.chat-sidebar-list li').on('click', function () {
            $('.chat-sidebar-list').find('.active').removeClass('active');
            if (!$(this).hasClass('active')) {
                if ($(this).attr('user_id') && parseInt($(this).attr('user_id')) > 0) {
                    loadContent($(this).attr('user_id'));
                }
                $(this).addClass('active');
            }
        });

        function loadContent(user_id) {
            showLoading(true);
            $.ajax({
                url: "{{ route('chat.box') }}",
                data: {user_id: user_id, _token: '{{ csrf_token() }}'},
                type: "GET",
                success: function (response) {
                    showLoading(false);
                    if (response.status) {
                        alert("error");
                    } else {
                        $('#chatbox').html('');
                        $('#chatbox').html(response);
                        $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));
                    }
                },
                error: function (error) {
                    showLoading(false);
                }
            });
        }

        function chatMessagesSend() {
            let message = $('input[name="chat_message"]').val(),
                user_id = $('.chat-sidebar-list .active').attr('user_id');
            socket.emit('new message', {
                'user_id': user_id,
                'sender_id': '{{ auth()->user()->id }}',
                'message': message,
                'type' : 'user',
                'cryptostore_session': '{{ $_COOKIE["cryptostore_session"] }}',
                '_token': '{{ csrf_token() }}'
            });

            $('.chat-content').append('<div class="chat">\n' +
                '                                        <div class="chat-body">\n' +
                '                                            <div class="chat-message">\n' +
                '                                                <p>' + message + '</p>\n' +
                '                                                <span class="chat-time">{{ date('d-m-Y h:m') }}</span>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                    </div>');
            $('input[name="chat_message"]').val('');
            $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));
        }

        socket.on('new message', function (response) {
            let user_id = $('.chat-sidebar-list .active').attr('user_id');
            if (parseInt(user_id) === parseInt(response.user_id)) {
                $('.chat-content').append('<div class="chat chat-left">\n' +
                    '                                        <div class="chat-body">\n' +
                    '                                            <div class="chat-message">\n' +
                    '                                                <p>' + response.content + '</p>\n' +
                    '                                                <span class="chat-time">{{ date('d-m-Y h:m') }}</span>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>');
                $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));
            }

            let chatList = "",
                status = false;
            $('.chat-sidebar-list li').each(function () {
                if (parseInt($(this).attr('user_id')) === parseInt(response.user_id)) {
                    status = true;
                }
                else {
                    chatList+=$(this).get(0).outerHTML;
                }
            });

               let addedItem = "<li user_id='"+response.user_id+"'>\n" +
                "                                    <div class=\"d-flex align-items-center\">\n" +
                "                                        <div class=\"chat-sidebar-name\">\n" +
                "                                            <h6 class=\"mb-0\">"+response.name + " " + response.surname +"</h6>\n" +
                "                                            <span class=\"text-muted\">"+response.email+"</span>\n" +
                "                                            <span\n" +
                "                                                class=\"user-status text text-success\">online</span>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </li>";

            addedItem += chatList;

            $('.chat-sidebar-list').html('');
            $('.chat-sidebar-list').html(addedItem);

            $('.chat-sidebar-list li').on('click', function () {
                $('.chat-sidebar-list').find('.active').removeClass('active');
                if (!$(this).hasClass('active')) {
                    if ($(this).attr('user_id') && parseInt($(this).attr('user_id')) > 0) {
                        loadContent($(this).attr('user_id'));
                    }
                    $(this).addClass('active');
                }
            });
        });

        socket.on('user online', function (response) {
            console.log(response);
            $('.chat-sidebar-list li').each(function () {
                if (parseInt($(this).attr('user_id')) === parseInt(response.user_id)) {
                    $(this).find('.user-status').text('online');
                    $(this).find('.user-status').removeClass('text-info');
                    $(this).find('.user-status').addClass('text-success');
                }
            })
        });

        socket.on('user offline', function (response) {
            console.log(response);
            $('.chat-sidebar-list li').each(function () {
                if (parseInt($(this).attr('user_id')) === parseInt(response.user_id)) {
                    $(this).find('.user-status').text(response.last_active_date);
                    $(this).find('.user-status').removeClass('text-success');
                    $(this).find('.user-status').addClass('text-info');
                }
            })
        });
    </script>
@endsection
@section('authenticated_css')
    <link rel="stylesheet" href="{{ asset('css/app-chat.min.css') }}">
@endsection
