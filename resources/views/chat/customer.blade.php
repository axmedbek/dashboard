@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body chat-application" style="margin-top: 80px;">
        <div class="row">
            <div class="col-md-12">
                <div class="chat-area">
                    <div class="chat-header"
                         style="background-color: white;padding: 10px;margin-bottom: 6px;border-radius: 4px;">
                        <header style="border-bottom: 0px !important;"
                                class="d-flex justify-content-between align-items-center border-bottom px-1 py-75">
                            <div class="d-flex align-items-center">
                                <h4 class="mb-0">Elizabeth Elliott</h4>
                            </div>
                        </header>
                    </div>
                    <!-- chat card start -->
                    <div class="card chat-wrapper shadow-none">
                        <div class="card-content">
                            <div class="card-body chat-container ps ps--active-y">
                                <div class="chat-content" style="height: 395px;max-height: 395px;overflow: auto">
                                    @foreach($messages as $message)
                                        @if (auth()->user()->id === $message['sender_id'])
                                            <div class="chat">
                                                <div class="chat-body">
                                                    <div class="chat-message">
                                                        <p>{{ $message['message'] }}</p>
                                                        <span class="chat-time">{{ date('h:m',strtotime($message['created_at'])) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="chat chat-left">
                                                <div class="chat-body">
                                                    <div class="chat-message">
                                                        <p>{{ $message['message'] }}</p>
                                                        <span class="chat-time">{{ date('h:m',strtotime($message['created_at'])) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="card-footer chat-footer border-top px-2 pt-1 pb-0 mb-1">
                            <form class="d-flex align-items-center" onsubmit="chatMessagesSend();"
                                  action="javascript:void(0);">
                                <i class="bx bx-face cursor-pointer"></i>
                                <i class="bx bx-paperclip ml-1 cursor-pointer"></i>
                                <input type="text" class="form-control chat-message-send mx-1"
                                       placeholder="{{__('messages.menu.chatSendMessagePH')}}" name="chat_message">
                                <button type="submit" class="btn btn-primary glow send d-lg-flex"><i
                                        class="bx bx-paper-plane"></i>
                                    <span class="d-none d-lg-block ml-1">{{__('messages.menu.send')}}</span></button>
                            </form>
                        </div>
                    </div>
                    <!-- chat card ends -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('authenticated_js')
    <script src="{{ asset('js/app-chat.min.js') }}"></script>
    <script>

        $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));

        function chatMessagesSend() {
            let message = $('input[name="chat_message"]').val();
            socket.emit('new message', {
                'user_id': 1,
                'sender_id': '{{ auth()->user()->id }}',
                'message': message,
                'type' : 'admin',
                'cryptostore_session': '{{ $_COOKIE["cryptostore_session"] }}',
                '_token': '{{ csrf_token() }}'
            });

            $('.chat-content').append('<div class="chat">\n' +
                '                                        <div class="chat-body">\n' +
                '                                            <div class="chat-message">\n' +
                '                                                <p>' + message + '</p>\n' +
                '                                                <span class="chat-time">{{ date('h:m') }}</span>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                    </div>');
            $('input[name="chat_message"]').val('');
            $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));
        }

        socket.on('new message', function (response) {
            $('.chat-content').append('<div class="chat chat-left">\n' +
                '                                        <div class="chat-body">\n' +
                '                                            <div class="chat-message">\n' +
                '                                                <p>' + response.content + '</p>\n' +
                '                                                <span class="chat-time">{{ date('h:m') }}</span>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                    </div>');
            $('.chat-content').scrollTop($('.chat-content').prop("scrollHeight"));
        });

    </script>
@endsection
@section('authenticated_css')
    <link rel="stylesheet" href="{{ asset('css/app-chat.min.css') }}">
@endsection
