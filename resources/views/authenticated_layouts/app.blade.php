@extends('layouts.app')

@section('content')
    <div class="header dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <nav class="navbar navbar-expand-lg navbar-light px-0 justify-content-between">
                        <a class="navbar-brand" href="/">
                            <span style="color: #5d78ff;font-weight: bold;font-size: 28px;">Crypto{{ auth()->user()->isAdmin() ? 'ADMIN' :  'store' }}.</span>
                        </a>

                        <div class="header-right d-flex my-2 align-items-center">
                            <div class="language">
                                <div class="dropdown">
                                    @if (app()->getLocale() === 'az')
                                        <div class="icon" data-toggle="dropdown">
                                            <i class="flag-icon flag-icon-az"></i>
                                            <span>Azerbaijan</span>
                                        </div>
                                    @elseif (app()->getLocale() === 'ru')
                                        <div class="icon" data-toggle="dropdown">
                                            <i class="flag-icon flag-icon-ru"></i>
                                            <span>Russian</span>
                                        </div>
                                    @else
                                        <div class="icon" data-toggle="dropdown">
                                            <i class="flag-icon flag-icon-us"></i>
                                            <span>English</span>
                                        </div>
                                    @endif
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if (app()->getLocale() === 'az')
                                            <a href="{{ route('change.lang','en') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-us"></i> English
                                            </a>
                                            <a href="{{ route('change.lang','ru') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-ru"></i> Russian
                                            </a>
                                        @elseif (app()->getLocale() === 'ru')
                                            <a href="{{ route('change.lang','en') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-us"></i> English
                                            </a>
                                            <a href="{{ route('change.lang','az') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-az"></i> Azerbaijan
                                            </a>
                                        @else
                                            <a href="{{ route('change.lang','ru') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-ru"></i> Russian
                                            </a>
                                            <a href="{{ route('change.lang','az') }}" class="dropdown-item">
                                                <i class="flag-icon flag-icon-az"></i> Azerbaijan
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="dashboard_log">
                                <div class="profile_log dropdown">
                                    <div class="user" data-toggle="dropdown">
                                        <span class="thumb"><i class="mdi mdi-account"></i></span>
                                        <span class="arrow"><i class="la la-angle-down"></i></span>
                                    </div>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <div class="user-email">
                                            <div class="user">
                                                <span class="thumb"><i class="mdi mdi-account"></i></span>
                                                <div class="user-info">
                                                    <h6>{{ auth()->user()->name }} {{ auth() ->user()->surname }}</h6>
                                                    <span>{{ auth()->user()->email }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        {{--                                        <div class="user-balance">--}}
                                        {{--                                            <div class="available">--}}
                                        {{--                                                <p>Available</p>--}}
                                        {{--                                                <span>0.00 USD</span>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="total">--}}
                                        {{--                                                <p>Total</p>--}}
                                        {{--                                                <span>0.00 USD</span>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        @if(auth()->user()->isAdmin() != true)
                                            <a href="{{ route('history') }}" class="dropdown-item">
                                                <i class="mdi mdi-history"></i> {{ __('messages.menu.history') }}
                                            </a>
                                        @endif

                                        {{--                                        <a href="data-tbi.html" class="dropdown-item">--}}
                                        {{--                                            <i class="mdi mdi-history"></i> History--}}
                                        {{--                                        </a>--}}
                                        <a href="{{ route('setting') }}" class="dropdown-item">
                                            <i class="mdi mdi-settings"></i> {{ __('messages.menu.setting') }}
                                        </a>
                                        {{--                                        <a href="lock.html" class="dropdown-item">--}}
                                        {{--                                            <i class="mdi mdi-lock"></i> Lock--}}
                                        {{--                                        </a>--}}
                                        <a href="{{ route('logout') }}" class="dropdown-item logout">
                                            <i class="mdi mdi-logout"></i> {{ __('messages.menu.logout') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="menu">
            <ul>
                <li>
                    <a href="/" data-toggle="tooltip" data-placement="right"
                       title="{{ __('messages.menu.dashboard') }}">
                        <span><i class="mdi mdi-view-dashboard"></i></span>
                    </a>
                </li>
                @if(auth()->user()->isAdmin() != true)
                    <li>
                        <a href="{{ route('history') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.history') }}">
                            <span><i class="mdi mdi-history"></i></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('balance') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.balance') }}">
                            <span><i class="mdi mdi-scale-balance"></i></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('chat') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.chat') }}">
                            <span><i class="mdi mdi-message-reply"></i></span>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('admin.users') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.users') }}">
                            <span><i class="mdi mdi-account-multiple"></i></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('permissions.admin') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.permissions') }}">
                            <span><i class="mdi mdi-lock"></i></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.history') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.history') }}">
                            <span><i class="mdi mdi-history"></i></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.chat') }}" data-toggle="tooltip" data-placement="right"
                           title="{{ __('messages.menu.chat') }}">
                            <span><i class="mdi mdi-message-reply"></i></span>
                        </a>
                    </li>
                @endif
                {{--                <li>--}}
                {{--                    <a href="{{ route('account') }}" data-toggle="tooltip" data-placement="right" title="Accounts">--}}
                {{--                        <span><i class="mdi mdi-face-profile"></i></span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li>--}}
                {{--                    <a href="{{ route('data') }}" data-toggle="tooltip" data-placement="right" title="Data">--}}
                {{--                        <span><i class="mdi mdi-database"></i></span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                <li>
                    <a href="{{ route('setting') }}" data-toggle="tooltip" data-placement="right"
                       title="{{ __('messages.menu.setting') }}">
                        <span><i class="mdi mdi-settings"></i></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @yield('authenticated_content')
    <div class="footer dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8 col-12">
                    <div class="copyright">
                        <p>© Copyright {{ date('Y') }} <a href="https://grinduck.com">grinduck</a> All
                            Rights Reserved</p>
                    </div>
                </div>
                <div class="col-sm-4 col-12">
                    <div class="footer-social">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('vendor/amchart/amcharts.js') }}"></script>
    <script src="{{ asset('vendor/amchart/serial.js') }}"></script>
    <script src="{{ asset('vendor/amchart/dataloader.min.js') }}"></script>
    <script src="{{ asset('js/plugins/amchart-init.js') }}"></script>
    <script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('vendor/circle-progress/circle-progress-init.js') }}"></script>
    @yield('authenticated_js')
@endsection

@section('css')
    @yield('authenticated_css')
@endsection
