@extends('authenticated_layouts.app')
@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container-fluid">
            <div class="price-grid row">
                <div class="col-xl-6 col-xxl-12 col-lg-12 col-xxl-6">
                    <div class="card">

                        @if (auth()->user()->isAdmin())
                            @include('admin')
                        @else
                            @include('customer')
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



