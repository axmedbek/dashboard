@section('authenticated_css')
    <style>

    </style>
@endsection
<div class="card-header">
    <h4 class="card-title">{{__('messages.menu.cryptocurrency')}}</h4>
</div>
<div class="card-body">
    <div class="row">
        <div class="col-md-3">
            <div class="card card-bordered card-full">
                <div class="card-inner" style="    background-color: #ff4141;
    color: white;
    border-radius: 6px;
">
                    <div class="card-title-group align-start mb-0">
                        <div class="row" style="width: 100%;">
                            <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                <i class="fa fa-bitcoin"></i>
                            </div>
                            <div class="col-md-8">
                                <h6 style="font-size: 15px;color: white;"
                                    class="subtitle">{{__('messages.menu.cryptocurrency')}}</h6>
                                <span
                                    style="font-size: 20px;">{{ \App\Balance::where('user_id',auth()->user()->id)->count() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-bordered card-full">
                <div class="card-inner" style="    background-color: #24ea8b;
    color: white;
    border-radius: 6px;
">
                    <div class="card-title-group align-start mb-0">
                        <div class="row" style="width: 100%;">
                            <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                <i class="mdi mdi-cash-usd"></i>
                            </div>
                            <div class="col-md-8">
                                <h6 style="font-size: 20px;color: white;"
                                    class="subtitle">{{__('messages.menu.sales')}}</h6>
                                <span
                                    style="font-size: 20px;">{{ \App\History::join('user_crypto_currencies','user_crypto_currencies.id','histories.user_crypto_currency_id')
->join('balances','balances.id','user_crypto_currencies.balance_id')
->where('histories.type','buy')->where('balances.user_id',auth()->user()->id)->count() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-bordered card-full">
                <div class="card-inner" style="    background-color: #798bff;
    color: white;
    border-radius: 6px;
">
                    <div class="card-title-group align-start mb-0">
                        <div class="row" style="width: 100%;">
                            <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                <i class="mdi mdi-coin"></i>
                            </div>
                            <div class="col-md-8">
                                <h6 style="font-size: 20px;color: white;"
                                    class="subtitle">{{__('messages.menu.purchases')}}</h6>
                                <span
                                    style="font-size: 20px;">{{ \App\History::join('user_crypto_currencies','user_crypto_currencies.id','histories.user_crypto_currency_id')
->join('balances','balances.id','user_crypto_currencies.balance_id')
->where('histories.type','sell')->where('balances.user_id',auth()->user()->id)->count() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-bordered card-full">
                <div class="card-inner" style="    background-color: #fe9431;
    color: white;
    border-radius: 6px;
">
                    <div class="card-title-group align-start mb-0">
                        <div class="row" style="width: 100%;">
                            <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                <i class="mdi mdi-message"></i>
                            </div>
                            <div class="col-md-8">
                                <h6 style="font-size: 20px;color: white;"
                                    class="subtitle">{{__('messages.menu.messages')}}</h6>
                                <span style="font-size: 20px;">{{ \App\Chat::where('user_id',auth()->user()->id)->count() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xxl-12" style="margin-top: 20px;"></div>
        <div class="col-xxl-12">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group">
                        <div class="card-title">
                            <h6 class="title"><span class="mr-2">{{__('messages.menu.lastHistory')}}</span>
                                <a href="{{ route('history') }}"
                                   class="link d-none d-sm-inline">{{__('messages.menu.showAll')}}</a>
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="card-inner p-0 border-top">
                    <div class="nk-tb-list nk-tb-orders">
                        <div class="nk-tb-item nk-tb-head">
                            <div class="nk-tb-col"><span>#</span></div>
                            <div class="nk-tb-col tb-col-md"><span>{{__('messages.menu.date')}}</span></div>
                            <div class="nk-tb-col tb-col-lg"><span>{{__('messages.menu.type')}}</span></div>
                            <div class="nk-tb-col"><span>{{__('messages.menu.amount')}}</span></div>
                            <div class="nk-tb-col"><span
                                    class="d-none d-sm-inline">{{__('messages.menu.cryptocurrency')}}</span></div>
                            <div class="nk-tb-col"><span>{{__('messages.menu.icon')}}</span></div>
                            <div class="nk-tb-col"><span>{{__('messages.menu.symbol')}}</span></div>
                            <div class="nk-tb-col"><span>{{__('messages.menu.payment')}}</span></div>
                        </div>
                        @php
                            use App\History;
$payments = History::join('user_crypto_currencies', 'user_crypto_currencies.id', 'histories.user_crypto_currency_id')
         ->join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
         ->join('users', 'users.id', 'balances.user_id')
         ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
         ->select('users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency', 'histories.type',
             'cryptocurrencies.symbol', 'histories.created_at as date', 'histories.amount', 'histories.payment')
             ->orderByDesc('histories.created_at')
             ->where('balances.user_id',auth()->user()->id)
                        ->take(6)->get();
                        @endphp
                        @foreach($payments as $key => $history)
                            <div class="nk-tb-item">
                                <div class="nk-tb-col"><span class="tb-lead">{{ ++$key }}</span></div>
                                <div class="nk-tb-col tb-col-md"><span class="tb-sub">{{ $history['date'] }}</span>
                                </div>
                                <div class="nk-tb-col tb-col-lg"><span
                                        class="tb-sub badge badge-{{ $history['type'] === 'buy' ? 'success' : 'danger' }}">{{ $history['type'] }}</span>
                                </div>
                                <div class="nk-tb-col"><span
                                        class="tb-sub tb-amount">{{ $history['amount'] }} <span></span></span>
                                </div>
                                <div class="nk-tb-col"><span
                                        class="tb-sub">{{ $history['cryptocurrency'] }} <span></span></span>
                                </div>
                                <div class="nk-tb-col"><span class="tb-sub">
                                             <img
                                                 src='{{ url('/images/coins/'.strtolower($history['symbol']).'.png') }}'
                                                 alt="{{ $history['cryptocurrency'] }} cryptostore">
                                            <span></span></span>
                                </div>
                                <div class="nk-tb-col"><span
                                        class="tb-sub">{{ $history['symbol'] }} <span></span></span>
                                </div>
                                <div class="nk-tb-col"><span
                                        class="tb-sub">{{ $history['payment'] }} <span> USD</span></span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xxl-12" style="margin-top: 20px;"></div>
        <div class="col-md-12">
            <div class="search-crypto-list">
                <input class="search-crypto-valute" placeholder="{{__('messages.menu.search')}}.." name="search">
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-hover table-bordered cyrpto-list">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('messages.menu.name')}}</th>
                    <th>{{__('messages.menu.symbol')}}</th>
                    <th>{{__('messages.menu.price')}}</th>
                    <th>{{__('messages.menu.percent_change_1h')}}</th>
                    <th>{{__('messages.menu.percent_change_24h')}}</th>
                    <th>{{__('messages.menu.percent_change_7d')}}</th>
                    <th>{{__('messages.menu.market_cap')}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach(\App\Cryptocurrency::all() as $key => $cryptocurrency)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>
                            <img style="width: 35px;"
                                 src='{{ url('/images/coins/'.strtolower($cryptocurrency['symbol']).'.png') }}'
                                 alt="{{ $cryptocurrency['name'] }} cryptostore">
                            {{ $cryptocurrency['name'] }}
                        </td>
                        <td>{{ $cryptocurrency['symbol'] }}</td>
                        <td>{{ $cryptocurrency['price'] }} USD</td>
                        <td>{{ $cryptocurrency['percent_change_1h'] }}</td>
                        <td>{{ $cryptocurrency['percent_change_24h'] }}</td>
                        <td>{{ $cryptocurrency['percent_change_7d'] }}</td>
                        <td>{{ $cryptocurrency['market_cap'] }}</td>
                        <td>
                            <a href="{{ route('chat') }}" class="btn btn-success waves-effect px-4">
                                {{__('messages.menu.buy')}}
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('authenticated_js')
    <script src="{{ asset('vendor/toastr/toastr-init.js') }}"></script>
    <script>
        $('.search-crypto-valute').on('keyup ', function () {
            var value = $(this).val().toLowerCase();

            $(".cyrpto-list tbody tr").filter(function (item) {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

    </script>
@endsection
