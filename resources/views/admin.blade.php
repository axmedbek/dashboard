<div class="card-header">
    <h4 class="card-title">#Dashboard</h4>
</div>
<div class="card-body">
    <div class="nk-block">
        <div class="row g-gs">
            <div class="col-md-3">
                <div class="card card-bordered card-full">
                    <div class="card-inner" style="    background-color: #ff4141;
    color: white;
    border-radius: 6px;
">
                        <div class="card-title-group align-start mb-0">
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                    <i class="mdi mdi-account-group"></i>
                                </div>
                                <div class="col-md-8">
                                    <h6 style="font-size: 20px;color: white;" class="subtitle">{{__('messages.menu.customers')}}</h6>
                                    <span
                                        style="font-size: 20px;">{{ \App\User::where('user_type','user')->count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-bordered card-full">
                    <div class="card-inner" style="    background-color: #24ea8b;
    color: white;
    border-radius: 6px;
">
                        <div class="card-title-group align-start mb-0">
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                    <i class="mdi mdi-cash-usd"></i>
                                </div>
                                <div class="col-md-8">
                                    <h6 style="font-size: 20px;color: white;" class="subtitle">{{__('messages.menu.sales')}}</h6>
                                    <span
                                        style="font-size: 20px;">{{ \App\History::where('type','buy')->count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-bordered card-full">
                    <div class="card-inner" style="    background-color: #798bff;
    color: white;
    border-radius: 6px;
">
                        <div class="card-title-group align-start mb-0">
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                    <i class="mdi mdi-coin"></i>
                                </div>
                                <div class="col-md-8">
                                    <h6 style="font-size: 20px;color: white;" class="subtitle">{{__('messages.menu.purchases')}}</h6>
                                    <span
                                        style="font-size: 20px;">{{ \App\History::where('type','sell')->count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-bordered card-full">
                    <div class="card-inner" style="    background-color: #fe9431;
    color: white;
    border-radius: 6px;
">
                        <div class="card-title-group align-start mb-0">
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4" style="    font-size: 40px;
    color: white;
    text-align: center;
    border-radius: 8px;">
                                    <i class="mdi mdi-message"></i>
                                </div>
                                <div class="col-md-8">
                                    <h6 style="font-size: 20px;color: white;" class="subtitle">{{__('messages.menu.messages')}}</h6>
                                    <span style="font-size: 20px;">{{ \App\Chat::count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xxl-12">
                <div class="card card-bordered card-full">
                    <div class="card-inner">
                        <div class="card-title-group">
                            <div class="card-title">
                                <h6 class="title"><span class="mr-2">{{__('messages.menu.lastHistory')}}</span>
                                    <a href="{{ route('admin.history') }}" class="link d-none d-sm-inline">{{__('messages.menu.showAll')}}</a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-inner p-0 border-top">
                        <div class="nk-tb-list nk-tb-orders">
                            <div class="nk-tb-item nk-tb-head">
                                <div class="nk-tb-col"><span>#</span></div>
                                <div class="nk-tb-col tb-col-sm"><span>{{__('messages.menu.customer')}}</span></div>
                                <div class="nk-tb-col tb-col-md"><span>{{__('messages.menu.date')}}</span></div>
                                <div class="nk-tb-col tb-col-lg"><span>{{__('messages.menu.type')}}</span></div>
                                <div class="nk-tb-col"><span>{{__('messages.menu.amount')}}</span></div>
                                <div class="nk-tb-col"><span class="d-none d-sm-inline">{{__('messages.menu.cryptocurrency')}}</span></div>
                                <div class="nk-tb-col"><span>{{__('messages.menu.icon')}}</span></div>
                                <div class="nk-tb-col"><span>{{__('messages.menu.symbol')}}</span></div>
                                <div class="nk-tb-col"><span>{{__('messages.menu.payment')}}</span></div>
                            </div>
                            @php
                                use App\History;
$payments = History::join('user_crypto_currencies', 'user_crypto_currencies.id', 'histories.user_crypto_currency_id')
             ->join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
             ->join('users', 'users.id', 'balances.user_id')
             ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
             ->select('users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency', 'histories.type',
                 'cryptocurrencies.symbol', 'histories.created_at as date', 'histories.amount', 'histories.payment')
                 ->orderByDesc('histories.created_at')
                            ->take(6)->get();
                            @endphp
                            @foreach($payments as $key => $history)
                                <div class="nk-tb-item">
                                    <div class="nk-tb-col"><span class="tb-lead">{{ ++$key }}</span></div>
                                    <div class="nk-tb-col tb-col-sm">
                                        <div class="user-card">
                                            <div class="user-avatar user-avatar-sm bg-purple">
                                                <span>{{ substr($history['name'],0,1) }}{{ substr($history['surname'],0,1) }}</span>
                                            </div>
                                            <div class="user-name"><span
                                                    class="tb-lead">{{ $history['name'] }} {{ $history['surname'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nk-tb-col tb-col-md"><span class="tb-sub">{{ $history['date'] }}</span>
                                    </div>
                                    <div class="nk-tb-col tb-col-lg"><span
                                            class="tb-sub badge badge-{{ $history['type'] === 'buy' ? 'success' : 'danger' }}">{{ $history['type'] }}</span>
                                    </div>
                                    <div class="nk-tb-col"><span
                                            class="tb-sub tb-amount">{{ $history['amount'] }} <span></span></span>
                                    </div>
                                    <div class="nk-tb-col"><span
                                            class="tb-sub">{{ $history['cryptocurrency'] }} <span></span></span>
                                    </div>
                                    <div class="nk-tb-col"><span class="tb-sub">
                                             <img
                                                 src='{{ url('/images/coins/'.strtolower($history['symbol']).'.png') }}'
                                                 alt="{{ $history['cryptocurrency'] }} cryptostore">
                                            <span></span></span>
                                    </div>
                                    <div class="nk-tb-col"><span
                                            class="tb-sub">{{ $history['symbol'] }} <span></span></span>
                                    </div>
                                    <div class="nk-tb-col"><span
                                            class="tb-sub">{{ $history['payment'] }} <span> USD</span></span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xxl-6">
                <div class="card card-bordered card-full">
                    <div class="card-inner border-bottom">
                        <div class="card-title-group">
                            <div class="card-title">
                                <h6 class="title">{{__('messages.menu.recentActivities')}}</h6></div>
                        </div>
                    </div>
                    <ul class="nk-activity">
                        @php
                            $messages = \App\Chat::join('users','users.id','chats.user_id')->take(5)->get();
                        @endphp
                        @foreach($messages as $message)
                            <li class="nk-activity-item">
                                <div
                                    class="nk-activity-media user-avatar bg-warning">{{ substr($message['name'],0,1) }}{{ substr($message['surname'],0,1) }}</div>
                                <div class="nk-activity-data">
                                    <div class="label">{{ $history['name'] }} {{ $history['surname'] }}</div>
                                    <span class="time">10 minutes ago</span></div>
                            </li>
                        @endforeach
                        @if (count($messages) < 1)
                            <li class="nk-activity-item">
                                {{__('messages.menu.notMessages')}}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-xxl-6">
                <div class="card card-bordered card-full">
                    <div class="card-inner-group">
                        <div class="card-inner">
                            <div class="card-title-group">
                                <div class="card-title">
                                    <h6 class="title">{{__('messages.menu.newCustomers')}}</h6></div>
                                <div class="card-tools"><a href="{{ route('admin.users') }}" class="link">{{__('messages.menu.showAll')}}</a></div>
                            </div>
                        </div>
                        @php
                            $users = \App\User::where('user_type','user')->orderByDesc('created_at')->take(5)->get();
                        @endphp
                        @foreach($users as $user)
                            <div class="card-inner card-inner-md">
                                <div class="user-card">
                                    <div class="user-avatar bg-primary-dim"><span style="color: white;">{{ substr($user['name'],0,1) }}{{ substr($user['surname'],0,1) }}</span></div>
                                    <div class="user-info"><span class="lead-text">{{ $user['name'] }} {{ $user['surname'] }}</span>
                                        <br>
                                        <span class="sub-text">{{ $user['email'] }}</span></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

