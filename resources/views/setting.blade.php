@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{__('messages.menu.changeYourPassword')}}</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('user.change.password') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.newPassword')}}</label>
                                                    <input type="password" class="form-control"
                                                           placeholder="{{__('messages.menu.newPassword')}}"
                                                           name="password" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.confirmNewPassword')}}</label>
                                                    <input type="password" class="form-control"
                                                           placeholder="{{__('messages.menu.confirmNewPassword')}}"
                                                           name="password_confirmation" required>
                                                </div>
                                            </div>

                                            <div class="col-12" style="margin-top: 20px;">
                                                <button class="btn btn-success waves-effect px-4">{{__('messages.menu.change')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{__('messages.menu.personalInfo')}}</h4>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('setting.user.update') }}" method="post"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.name')}}</label>
                                                    <input type="text" class="form-control" placeholder="Name"
                                                           name="name"
                                                           required value="{{ auth()->user()->name }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.surname')}}</label>
                                                    <input type="text" class="form-control" placeholder="Surname"
                                                           name="surname"
                                                           required value="{{ auth()->user()->surname }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.email')}}</label>
                                                    <input type="email" class="form-control" placeholder="Email"
                                                           name="email"
                                                           required value="{{ auth()->user()->email }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: grid;">
                                                    <label>{{__('messages.menu.phone')}}</label>
                                                    <input id="phone-selector" type="text" class="form-control"
                                                           name="phone" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" bis_skin_checked="1">
                                                    <label class="mr-sm-2">{{__('messages.menu.uploadID')}}</label>
                                                    <span class="float-right">{{__('messages.menu.maxFileSize2')}}</span>
                                                    <div class="file-upload-wrapper" data-text="id.jpg"
                                                         bis_skin_checked="1">
                                                        <input id="image-add" name="images[]" type="file"
                                                               class="file-upload-field" multiple
                                                               accept="image/png, image/jpeg, image/jpg">
                                                    </div>
                                                    <div class="row image-shower">
                                                        @foreach(auth()->user()->images as $image)
                                                            <div image-id="{{ $image['id'] }}"
                                                                 class="id-image-element col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img
                                                                            style="width: 100%;height: 250px;object-fit: contain;"
                                                                            src="{{ url("/images/id-images/".$image->path) }}"
                                                                            alt="loaded img">
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="btn-group" role="group"
                                                                             aria-label="Basic example"
                                                                             style="display: flex;">
                                                                            <button style="margin-top: 0;" type="button"
                                                                                    class="btn btn-danger btn-block"
                                                                                    onclick="deleteImage(this)">{{__('messages.menu.delete')}}
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12" style="margin-top: 20px;">
                                                <button class="btn btn-success waves-effect px-4">{{__('messages.menu.update')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('authenticated_css')
    <link rel="stylesheet" href="{{ asset('vendor/phone-selector/css/intlTelInput.min.css') }}">
    <style>
        .iti__flag {
            background-image: url("/vendor/phone-selector/img/flags.png");
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url("/vendor/phone-selector/img/flags@2x.png");
            }
        }
    </style>
@endsection
@section('authenticated_js')
    <script src="{{ asset('vendor/phone-selector/js/intlTelInput.min.js') }}"></script>
    <script>
        var phone = $("#phone-selector").val();
        var input = document.querySelector("#phone-selector");

        window.intlTelInput(input, {
            autoPlaceholder: "aggressive",
            separateDialCode: true,
            placeholderNumberType: "FIXED_LINE",
            preferredCountries : ['az','us','ru'],
        }).setNumber("+"+{{ auth()->user()->phone }});


        $("form").submit(function () {
            var countryCode = $('.iti__selected-flag .iti__selected-dial-code').html();
            countryCode = countryCode.split('+')[1]
            var phone = $("#phone-selector").val();
            $("#phone-selector").val(countryCode + phone);
        });

        // Multiple images preview in browser
        let imagesPreview = function (input, placeToInsertImagePreview) {
            if (input.files) {
                let filesAmount = input.files.length;
                if (parseInt($('.id-image-element').length) >= 2 || filesAmount > 2) {
                    toastr.warning("{{__('messages.menu.maxFileSize2')}},{{__('messages.menu.warning')}}");
                    return;
                }

                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (event) {
                        $('<div class="id-image-element col-md-6">' +
                            '<div class="row">' +
                            '<div class="col-md-12">' +
                            '<img style="width: 100%;height: 250px;object-fit: contain;" src="' + event.target.result + '" alt="loaded img">' +
                            '</div>' +
                            '<div class="col-md-12">' +
                            '<div class="btn-group" role="group" aria-label="Basic example" style="display: flex;">\n' +
                            '  <button style="margin-top: 0;" type="button" class="btn btn-danger btn-block" onclick="deleteImage(this)">Delete</button>\n' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>').appendTo(placeToInsertImagePreview);
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#image-add').on('change', function () {
            imagesPreview(this, 'div.image-shower');
        });

        function deleteImage(element) {
            let id = $(element).parents('.id-image-element:eq(0)').attr('image-id');

            if (id) {
                Swal.fire({
                    title: 'Warning!',
                    text: "Do you want delete this image",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{__('messages.menu.cancelButton')}}',
                    confirmButtonText: '{{__('messages.menu.confirmButton')}}'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: '{{ route('setting.user.delete.image') }}',
                            data: {_token: '{{ csrf_token() }}', id: id},
                            type: "POST",
                            success: function (response) {
                                if (response.status) {
                                    Swal.fire({
                                        icon: 'success',
                                        text: 'Operation successfully'
                                    });
                                    $(element).parents('div:eq(3)').remove();
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        text: 'Oops.Problem happened.'
                                    })
                                }
                            }
                        });
                    }
                });
            }
            else {
                $(element).parents('div:eq(3)').remove();
            }
        }
    </script>
@stop
