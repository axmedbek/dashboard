@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{__('messages.menu.allBalances')}}</h4>
                        </div>
                        <div class="card-body">
                            <div class="transaction-table">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0 table-responsive-sm">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{__('messages.menu.amount')}}</th>
                                            <th>{{__('messages.menu.cryptocurrency')}}</th>
                                            <th>{{__('messages.menu.icon')}}</th>
                                            <th>{{__('messages.menu.symbol')}}</th>
                                            <th>{{__('messages.menu.buyPrice')}}</th>
                                            <th>{{__('messages.menu.currentPrice')}}</th>
                                            <th>{{__('messages.menu.yourProfit')}}</th>
                                            <th>{{ __('messages.menu.date') }}</th>
                                            <th>{{ __('messages.menu.operation') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($balances as $key => $balance)
                                            <tr>
                                                <th scope="row">{{ $balances->firstItem() + $key }}</th>
                                                <td>{{ $balance['amount'] }}</td>
                                                <td>
                                                    {{ $balance['cryptocurrency'] }}
                                                </td>
                                                <td>
                                                    <img src='{{ url('/images/coins/'.strtolower($balance['symbol']).'.png') }}' alt="{{ $balance['cryptocurrency'] }} cryptostore">
                                                </td>
                                                <td>{{ $balance['symbol'] }}</td>
                                                <td>{{ $balance['buy_cost'] }} USD</td>
                                                <td>
                                                    <span style="padding: 10px;font-size: 15px;"
                                                        class="badge badge-{{ $balance['amount']*$balance['price'] > $balance['buy_cost'] ? 'success' : 'danger' }}">{{ $balance['amount']*$balance['price'] }} USD</span>
                                                </td>
                                                <td>
                                                    <span style="padding: 10px;font-size: 15px;"
                                                          class="badge badge-{{ $balance['amount']*$balance['price'] - $balance['buy_cost'] > 0 ? 'success' : 'danger' }}">
                                                        {{ $balance['amount']*$balance['price']-$balance['buy_cost'] }} USD
                                                    </span>
                                                </td>
                                                <td>{{ $balance['date'] }}</td>
                                                <td>
                                                    <a class="btn btn-danger" href="{{ route('chat') }}"> Sell</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
