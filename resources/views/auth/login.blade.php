@extends('layouts.app')

@section('content')
    <div class="authincation section-padding" style="margin-top: -5%">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="mini-logo text-center my-5">
                        <a class="navbar-brand" href="{{route('landing.page')}}">
                            <span style="color: #5d78ff;font-weight: bold;font-size: 28px;">Cryptostore.</span>
                        </a>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="auth-form card">
                        <div class="card-header justify-content-center">
                            <h4 class="card-title">Sign in</h4>
                        </div>
                        <div class="card-body">
                            <form method="post" name="myform" class="signin_validate" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="hello@example.com"
                                           name="email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password"
                                           name="password">
                                </div>
                                <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                    <div class="form-group mb-0">
                                        <label class="toggle">
                                            <input class="toggle-checkbox" type="checkbox">
                                            <span class="toggle-switch"></span>
                                            <span class="toggle-label">Remember me</span>
                                        </label>
                                    </div>
                                    <div class="form-group mb-0">
                                        <a href="{{ route('password.email') }}">Forgot Password?</a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                </div>
                            </form>
                            <div class="new-account mt-3">
                                <p>Don't have an account? <a class="text-primary" href="{{ route('register') }}">Sign
                                        up</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
