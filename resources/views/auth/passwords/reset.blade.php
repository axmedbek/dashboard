@extends('layouts.app')

@section('content')
    <div class="authincation section-padding" style="margin-top: -8%">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="mini-logo text-center my-5">
                        <a class="navbar-brand" href="/">
                            <span style="color: #5d78ff;font-weight: bold;font-size: 28px;">Cryptostore.</span>
                        </a>
                    </div>
                    <div class="auth-form card">
                        <div class="card-header justify-content-center">
                            <h4 class="card-title">Reset password</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('password.send.link') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="hello@example.com" name="email">
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success btn-block">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
