@extends('layouts.app')

@section('content')
    <div class="authincation section-padding" style="margin-top: -8%">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-xl-5 col-md-6">
                    <div class="mini-logo text-center my-5" style="margin-bottom: 10px !important;
    margin-top: 25px !important;">
                        <a class="navbar-brand" href="{{route('landing.page')}}">
                            <span style="color: #5d78ff;font-weight: bold;font-size: 28px;">Cryptostore.</span>
                        </a>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="auth-form card">
                        <div class="card-header justify-content-center">
                            <h4 class="card-title">Sign up your account</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('register') }}" method="post" name="myform" class="signup_validate"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="Name" name="name"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Surname</label>
                                            <input type="text" class="form-control" placeholder="Surname" name="surname"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" bis_skin_checked="1">
                                    <label class="mr-sm-2">Upload ID </label>
                                    <span class="float-right">Maximum file size is 2mb</span>
                                    <div class="file-upload-wrapper" data-text="id.jpg" bis_skin_checked="1">
                                        <input id="image-add" name="images[]" type="file"
                                               class="file-upload-field" multiple required
                                               accept="image/png, image/jpeg, image/jpg">
                                    </div>
                                    <div class="row image-shower">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="hello@example.com"
                                           name="email" required>
                                </div>
                                <div class="form-group" style="display: grid;">
                                    <label>Phone</label>
                                    <input id="phone-selector" type="number" class="form-control"
                                           name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password"
                                           name="password" required>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Confirm Password"
                                           name="password_confirmation" required>
                                </div>
                                <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-success btn-block">Sign up</button>
                                </div>
                            </form>
                            <div class="new-account mt-3">
                                <p>Already have an account? <a class="text-primary" href="{{ route('login') }}">Sign
                                        in</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/phone-selector/css/intlTelInput.min.css') }}">
    <style>
        .iti__flag {
            background-image: url("/vendor/phone-selector/img/flags.png");
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url("/vendor/phone-selector/img/flags@2x.png");
            }
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('vendor/phone-selector/js/intlTelInput.min.js') }}"></script>
    <script>

        var input = document.querySelector("#phone-selector");
        window.intlTelInput(input, {
            initialCountry: "az",
            separateDialCode: true,
            autoPlaceholder: "aggressive",
            placeholderNumberType: "FIXED_LINE"
        });
        // Multiple images preview in browser
        let imagesPreview = function (input, placeToInsertImagePreview) {
            if (input.files) {
                let filesAmount = input.files.length;
                if (parseInt($('.id-image-element').length) >= 2 || filesAmount > 2) {
                    toastr.warning('You can upload max 2 files.', 'Warning!');
                    return;
                }

                for (let i = 0; i < filesAmount; i++) {
                    let reader = new FileReader();

                    reader.onload = function (event) {
                        $('<div class="id-image-element col-md-6">' +
                            '<div class="row">' +
                            '<div class="col-md-12">' +
                            '<img style="width: 100%;height: 250px;object-fit: contain;" src="' + event.target.result + '" alt="loaded img">' +
                            '</div>' +
                            '<div class="col-md-12">' +
                            '<div class="btn-group" role="group" aria-label="Basic example" style="display: flex;">\n' +
                            '  <button style="margin-top: 0;" type="button" class="btn btn-danger btn-block" onclick="deleteImage(this)">Delete</button>\n' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>').appendTo(placeToInsertImagePreview);
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#image-add').on('change', function () {
            imagesPreview(this, 'div.image-shower');
        });

        function deleteImage(element) {
            $(element).parents('div:eq(3)').remove();
        }
    </script>
@stop
