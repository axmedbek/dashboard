@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row" style="width: 100%;">
                                <div class="col-md-4">
                                    <h4 class="card-title" style="margin-top: 5px;">Total balance  <span
                                            style="padding: 6px;
                                                    border-radius: 4px;
                                                    background-color: #5d78ff;
                                                    color: white;"
                                        ><i class="mdi mdi-account"></i> {{ $user['name'] }} {{ $user['surname'] }}</span></h4>
                                </div>
                                <div class="col-md-8" style="text-align: end;">
                                    <a class="btn btn-info" href="{{ route('admin.users') }}"><i class="mdi mdi-flip-to-back"></i> Users</a>
                                    @if(\App\Helpers\Standard::checkPermissionStatus('admin.balance',\App\Helpers\Standard::FULL_PERMISSION))
                                        <a class="btn btn-success" href="{{ route('admin.balance.add',$user['id'])}}"><i
                                                class="mdi mdi-scale-balance"></i> {{__('messages.menu.balance_add')}}
                                        </a>
                                    @endif
                                    <a class="btn btn-warning" href="{{ route('admin.users.balance',$user['id'])}}"><i class="mdi mdi-scale-balance"></i> Balance</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="permissions_table" class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Total amount</th>
                                        <th>Cryptocurrency</th>
                                        <th>Icon</th>
                                        <th>Symbol</th>
                                        <th>Total payment</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($balances as $key => $balance)
                                        <tr>
                                            <th scope="row">{{ $balances->firstItem() + $key }}</th>
                                            <td>{{ $balance['amount'] }}</td>
                                            <td>
                                                {{ $balance['cryptocurrency'] }}
                                            </td>
                                            <td>
                                                <img src='{{ url('/images/coins/'.strtolower($balance['symbol']).'.png') }}' alt="{{ $balance['cryptocurrency'] }} cryptostore">
                                            </td>
                                            <td>{{ $balance['symbol'] }}</td>
                                            <td>{{ $balance['payment'] }} USD</td>
                                            <td>{{ $balance['date'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            {{ $balances->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('authenticated_js')
    <script>

    </script>
@stop
