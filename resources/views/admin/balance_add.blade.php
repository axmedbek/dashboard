@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row" style="width: 100%;">
                                        <div class="col-md-4">
                                            <h4 class="card-title" style="margin-top: 5px;">{{__('messages.menu.balance_add')}}</h4>
                                        </div>
                                        <div class="col-md-8" style="text-align: end;">
                                            <a class="btn btn-info" href="{{ url()->previous() }}"><i class="mdi mdi-arrow-left"></i>
                                                {{ __('messages.menu.balance') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('admin.balance.save') }}" method="post">
                                        <input type="hidden" name="user_id" value="{{ $user_id }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.amount')}}</label>
                                                    <input type="number" class="form-control" placeholder="Amount"
                                                           name="amount" value="{{ old('amount') }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.cryptocurrency')}}</label>
                                                    <select class="form-control" name="cryptocurrency"
                                                            id="cryptocurrency">
                                                        @foreach(\App\Cryptocurrency::all() as $cryptocurrency)
                                                            <option {{ old('cryptocurrency') == $cryptocurrency['id'] ? 'selected' : '' }}
                                                                value="{{ $cryptocurrency['id'] }}">{{ $cryptocurrency['name'] }}</option>
                                                        @endforeach
                                                    </select>
                                                    <style type="text/css">
                                                        .nice-select .list {
                                                            height: 200px !important;
                                                            overflow: auto !important;
                                                        }
                                                    </style>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.payment')}}</label>
                                                    <div class="input-group mb-3" bis_skin_checked="1">
                                                        <div class="input-group-prepend" bis_skin_checked="1">
                                                            <span class="input-group-text">USD</span>
                                                        </div>
                                                        <input type="number" class="form-control" placeholder="Payment"
                                                               name="payment" value="{{ old('payment') }}" step=".001">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12" style="margin-top: 20px;">
                                                <button class="btn btn-success waves-effect px-4">
                                                    {{__('messages.menu.addUser')}}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
