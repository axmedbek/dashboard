@extends('authenticated_layouts.app')

@section('authenticated_content')

    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row" style="width: 100%;">
                                        <div class="col-md-4">
                                            <h4 class="card-title">{{__('messages.menu.personalInfo')}}</h4>
                                        </div>
                                        <div class="col-md-8" style="text-align: end;">
                                            <a class="btn btn-info" href="{{ route('admin.users') }}"><i class="mdi mdi-flip-to-back"></i> {{__('messages.menu.users')}} </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form action="#">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.name')}}</label>
                                                    <h4>{{$user->name}}</h4>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.surname')}}</label>
                                                    <h4>{{ $user->surname }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.email')}}</label>
                                                    <h4>{{ $user->email }}</h4>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: grid;">
                                                    <label>{{__('messages.menu.phone')}}</label>
                                                    <h4>{{ $user->phone }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row image-shower">
                                            @foreach($user->images as $image)
                                                <div class="id-image-element col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <img
                                                                style="width: 100%;height: 250px;object-fit: contain;"
                                                                src="{{ url("/images/id-images/".$image->path) }}"
                                                                alt="loaded img">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.edit-user-info').on('click', function (e) {
            e.stopPropagation();
            e.preventDefault();

            var name = $("input[name='name']").val();
            var surname  = $("input[name='surname']").val();
            var phone    = $("input[name='phone']").val();
            var email    = $("input[name='email']").val();
            var password = $("input[name='password']").val();


            var formData = {};
            formData.name = name;
            formData.surname = surname;
            formData.phone = phone;
            formData.email = email;
            formData.password = password;
            formData._token = "{{ csrf_token() }}";

            $.ajax({
                url: '{{ route("admin.users.updateInfo",$user['id']) }}',
                data: formData,
                type: "PUT",
                // processData: false,
                // contentType: false,
                success: function (response) {
                    if(response.status == true){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '{{__('messages.menu.successMessages')}}',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }

                }
            });
        })
    </script>
@endsection
