@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="add-user col-md-1">
                    <div>
                        @if(\App\Helpers\Standard::checkPermissionStatus('admin.users'))
                            <button class="btn btn-success waves-effect px-4 add-permission-user">
                                <a href="{{route('admin.users.add')}}">Add</a>
                            </button>
                        @endif
                    </div>
                </div>
                @if(\App\Helpers\Standard::checkPermissionStatus('admin.users'))
                    <div class="col-md-11 search-crypto-list">
                        <input class="search-crypto-valute" placeholder="{{__('messages.menu.search')}}.."
                               name="search">
                    </div>
                @else
                    <div class="col-md-12 search-crypto-list">
                        <input class="search-crypto-valute" placeholder="{{__('messages.menu.search')}}.."
                               name="search">
                    </div>
                @endif

            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="tab">
                    <button class="tablinks customer active">{{__('messages.menu.customer')}}</button>
                    <button class="tablinks syster-users">{{__('messages.menu.systemUsers')}}</button>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('messages.menu.name')}}</th>
                        <th>{{__('messages.menu.phone')}}</th>
                        <th>{{__('messages.menu.email')}}</th>
                        <th>{{__('messages.menu.authenticate')}}</th>
                        <th>{{__('messages.menu.balance')}}</th>
                        <th>{{__('messages.menu.view')}}</th>
                        @if(\App\Helpers\Standard::checkPermissionStatus('admin.users'))
                            <th>{{__('messages.menu.edit')}}</th>
                            <th>{{__('messages.menu.delete')}}</th>
                            <th>{{__('messages.menu.status')}}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody class="users">
                    @foreach($users as $indexKey => $user)
                        <tr name="{{$user->user_type}}" id="{{$user->id}}">
                            <td><span class="list-img">{{$users->firstItem() + $indexKey }}</span>
                            </td>
                            <td><a href="#"><span class="list-enq-name">  {{$user->name.' '.$user->surname}}</span></a>
                            </td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->email}}</td>
                            <td><span
                                    style="color: {{$user->email_verified_at != NULL ? 'green' : 'red'}}">{{ $user->email_verified_at != NULL ? __('messages.menu.approved') : __('messages.menu.notApproved')}}</span>
                            </td>

                            <td>
                                @if(\App\Helpers\Standard::checkPermissionStatus('admin.balance',\App\Helpers\Standard::READ_PERMISSION))
                                    <a href="{{route('admin.users.balance',$user->id)}}"><i class="fa fa-money"
                                                                                            aria-hidden="true"></i></a>
                                @else
                                    <span class="badge badge-danger">Deny permission</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.users.info',$user->id)}}"><i class="fa fa-eye"
                                                                                     aria-hidden="true"></i></a>
                            </td>
                            @if(\App\Helpers\Standard::checkPermissionStatus('admin.users'))
                                <td>
                                    <a href="{{route('admin.users.edit',$user->id)}}"><i class="fa fa-pencil-square-o"
                                                                                         aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    <a href="#" class="user-deleted"><i class="fa fa-trash-o"
                                                                        aria-hidden="true"></i></a>
                                </td>
                                <td><a href="#" class="user-blocked"
                                       name="{{$user->is_blocked == false ? 'blocked' : 'unblocked'}}"
                                       style="color: {{$user->is_blocked == false ? 'red' : 'green'}}">{{ $user->is_blocked == false ? __('messages.menu.blocked') : __('messages.menu.unblock')}}</a>
                                </td>

                            @endif

                        </tr>
                    @endforeach
                    </tbody>
                    <tbody class="system-users">
                    @foreach($systemUsers as $indexKey => $systemUser)
                        <tr name="{{$systemUser->user_type}}" id="{{$systemUser->id}}">
                            <td><span class="list-img">{{$systemUsers->firstItem() + $indexKey }}</span>
                            </td>
                            <td><a href="#"><span
                                        class="list-enq-name">  {{$systemUser->name.' '.$systemUser->surname}}</span></a>
                            </td>
                            <td>{{$systemUser->phone}}</td>
                            <td>{{$systemUser->email}}</td>
                            <td><span
                                    style="color: {{$user->email_verified_at != NULL ? 'green' : 'red'}}">{{ $systemUser->email_verified_at != NULL ? __('messages.menu.approved') : __('messages.menu.notApproved')}}</span>
                            </td>

                            <td>
                                -
                            </td>
                            @if(\App\Helpers\Standard::checkPermissionStatus('admin.users'))
                                <td>
                                    <a href="{{route('admin.users.info',$systemUser->id)}}"><i class="fa fa-eye"
                                                                                               aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    <a href="{{route('admin.users.edit',$systemUser->id)}}"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </td>
                                <td>
                                    <a href="#" class="user-deleted"><i class="fa fa-trash-o"
                                                                        aria-hidden="true"></i></a>
                                </td>
                                <td><a href="#" class="user-blocked"
                                       name="{{$systemUser->is_blocked == false ? 'blocked' : 'unblocked'}}"
                                       style="color: {{$systemUser->is_blocked == false ? 'red' : 'green'}}">{{ $systemUser->is_blocked == false ? __('messages.menu.blocked') : __('messages.menu.unblock')}}</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="user-list-page">{{$users->links()}}</div>

            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $('.search-crypto-valute').on('keyup ', function () {
            var value = $(this).val().toLowerCase();

            $(".table-hover tbody tr").filter(function (item) {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

        $(document).on('click', '.user-blocked', function () {
            var thisValue = $(this);
            var user_id = thisValue.parents('tr').attr('id');
            var url = '{{ route("admin.users.block", ":user_id") }}';
            url = url.replace(':user_id', user_id);
            var modalText = '';

            if ($(this).attr('name') == 'blocked') {
                modalText = "{{ __('messages.menu.blockedText')}}";
            } else {
                modalText = "{{ __('messages.menu.unblockedText')}}";
            }

            Swal.fire({
                title: 'Diqqət!',
                text: modalText,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: '{{__('messages.menu.cancelButton')}}',
                confirmButtonText: '{{__('messages.menu.confirmButton')}}'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        data: {_token: '{{ csrf_token() }}'},
                        type: "PUT",
                        success: function (response) {
                            if (response.data == true) {
                                thisValue.parents('td').html('<a href="#" class="user-blocked" style="color: green">{{ __('messages.menu.unblock')}}</a>');
                            } else {
                                thisValue.parents('td').html('<a href="#" class="user-blocked" style="color: red">{{ __('messages.menu.blocked')}}</a>');
                            }
                        }
                    });
                }
            })
        });

        $(document).on('click', '.user-deleted', function () {
            var thisValue = $(this);
            var user_id = $(this).parents('tr').attr('id');
            var url = '{{ route("admin.users.delete", ":user_id") }}';
            url = url.replace(':user_id', user_id);

            Swal.fire({
                title: 'Diqqət!',
                text: "{{__('messages.menu.deletedText')}}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: '{{__('messages.menu.cancelButton')}}',
                confirmButtonText: '{{__('messages.menu.confirmButton')}}'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        data: {_token: '{{ csrf_token() }}'},
                        type: "DELETE",
                        success: function (response) {
                            if (response.status == true) {
                                thisValue.parents('tr').remove();
                            }
                        }

                    });

                    Swal.fire(
                        'Blok edildi!'
                    )
                }
            })
        })

        $('.tablinks').on('click', function () {
            $('.tablinks').removeClass('active');
            $(this).addClass('active');

            if ($(this).hasClass('customer') && $(this).hasClass('active')) {
                $(".users").show()
                $(".system-users").hide()
            } else {
                $(".users").hide()
                $(".system-users").show()
            }
        })

    </script>
@endsection
