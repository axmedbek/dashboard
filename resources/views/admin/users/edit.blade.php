@extends('authenticated_layouts.app')

@section('authenticated_content')

    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row" style="width: 100%;">
                                        <div class="col-md-4">
                                            <h4 class="card-title">{{__('messages.menu.personalInfo')}}</h4>
                                        </div>
                                        <div class="col-md-8" style="text-align: end;">
                                            <a class="btn btn-info" href="{{ route('admin.users') }}"><i class="mdi mdi-flip-to-back"></i> {{__('messages.menu.users')}} </a>
                                         </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('admin.users.updateInfo',$user->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.name')}}</label>
                                                    <input type="text" class="form-control" placeholder="Name"
                                                           name="name"
                                                           required value="{{$user->name}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.surname')}}</label>
                                                    <input type="text" class="form-control" placeholder="Surname"
                                                           name="surname"
                                                           required value="{{ $user->surname }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{__('messages.menu.email')}}</label>
                                                    <input type="email" class="form-control" placeholder="Email"
                                                           name="email"
                                                           required value="{{ $user->email }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display: grid;">
                                                    <label>{{__('messages.menu.phone')}}</label>
                                                    <input id="phone-selector" type="text" class="form-control"
                                                           name="phone" required>
                                                </div>
                                            </div>
                                        </div>
                                        @if($user->user_type == 'admin')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Permission</label>
                                                    <select value="asd"  multiple class="form-control" id="permission" required
                                                            name="permission">
                                                        @foreach($permissions as $permission)
                                                            @if($permission->id == $user->permission_group_id)
                                                                <option selected value="{{$permission->id}}">{{$permission->name}}</option>
                                                            @else
                                                                <option value="{{$permission->id}}">{{$permission->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> {{__('messages.menu.newPassword')}}</label>
                                                    <input type="password" class="form-control"
                                                           placeholder="New password"
                                                           name="password">
                                                </div>
                                            </div>
                                            <div class="col-12" style="margin-top: 20px;">
                                                <button class="btn btn-success waves-effect px-4 edit-user-info">
                                                    {{__('messages.menu.change')}}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('authenticated_css')
    <link rel="stylesheet" href="{{ asset('vendor/phone-selector/css/intlTelInput.min.css') }}">
    <style>
        .iti__flag {
            background-image: url("/vendor/phone-selector/img/flags.png");
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url("/vendor/phone-selector/img/flags@2x.png");
            }
        }
    </style>
@endsection
@section('authenticated_js')
    <script src="{{ asset('vendor/phone-selector/js/intlTelInput.min.js') }}"></script>
    <script>

        var phone = $("#phone-selector").val();
        var input = document.querySelector("#phone-selector");

        window.intlTelInput(input, {
            autoPlaceholder: "aggressive",
            separateDialCode: true,
            placeholderNumberType: "FIXED_LINE",
            preferredCountries : ['az','us','ru'],
        }).setNumber("+"+{{ $user->phone }});


        $("form").submit(function () {
            var countryCode = $('.iti__selected-flag .iti__selected-dial-code').html();
            countryCode = countryCode.split('+')[1]
            var phone = $("#phone-selector").val();
            $("#phone-selector").val(countryCode + phone);
        });

    </script>
@endsection
