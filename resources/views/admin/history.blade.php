@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="display: flex">
                                <h4 class="card-title" style="margin-top: 5px;">{{__('messages.menu.history')}}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="">
                                <table id="permissions_table" class="table table-hover mb-0">
                                    <tr>
                                        <td>#</td>
                                        <td>
                                            <select name="user" id="user" class="form-control">
                                                <option value="none">Choose a option</option>
                                                @foreach(\App\User::where('user_type','user')->get() as $userItem)
                                                    <option
                                                        {{ isset($user) ? $user == $userItem['id'] ? 'selected' : '' : '' }} value="{{ $userItem['id'] }}">{{ $userItem['name'] }} {{ $userItem['surname'] }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="type" id="type" class="form-control">
                                                <option value="none">Choose a option</option>
                                                <option {{ isset($type) ? $type == 'buy' ? 'selected' : '' : ''}} value="buy">
                                                    Buy
                                                </option>
                                                <option
                                                    {{ isset($type) ? $type == 'sell' ? 'selected' : '' : '' }} value="sell">
                                                    Sell
                                                </option>
                                            </select>
                                        </td>
                                        <td></td>
                                        <td>
                                            <select name="crypto" id="crypto" class="form-control">
                                                <option value="none">Choose a option</option>
                                                @foreach(\App\Cryptocurrency::all() as $cryptocurrency)
                                                    <option
                                                        {{ isset($crypto) ? (int)$crypto == (int)$cryptocurrency['id'] ? 'selected' : '' : '' }} value="{{ $cryptocurrency['id'] }}">{{ $cryptocurrency['name'] }}</option>
                                                @endforeach
                                            </select>
                                            <style type="text/css">
                                                .nice-select .list {
                                                    height: 200px !important;
                                                    overflow: auto !important;
                                                }
                                            </style>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <input type="date" name="date" class="form-control"
                                                   value="{{ isset($date) ? $date : '' }}">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-info">Filter</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="permissions_table" class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('messages.menu.users')}}</th>
                                        <th>{{__('messages.menu.type')}}</th>
                                        <th>{{__('messages.menu.amount')}}</th>
                                        <th>{{__('messages.menu.cryptocurrency')}}</th>
                                        <th>{{__('messages.menu.icon')}}</th>
                                        <th>{{__('messages.menu.symbol')}}</th>
                                        <th>{{__('messages.menu.payment')}}</th>
                                        <th>{{__('messages.menu.date')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments as $key => $payment)
                                        <tr>
                                            <th scope="row">{{ $payments->firstItem() + $key }}</th>
                                            <td>{{ $payment['name'] }} {{ $payment['surname'] }}</td>
                                            <td>
                                                @if ($payment['type'] === 'buy')
                                                    <span class="badge badge-success">{{__('messages.menu.buy')}}</span>
                                                @else
                                                    <span class="badge badge-danger">{{__('messages.menu.sell')}}</span>
                                                @endif
                                            </td>
                                            <td>{{ $payment['amount'] }}</td>
                                            <td>
                                                {{ $payment['cryptocurrency'] }}
                                            </td>
                                            <td>
                                                <img
                                                    src='{{ url('/images/coins/'.strtolower($payment['symbol']).'.png') }}'
                                                    alt="{{ $payment['cryptocurrency'] }} cryptostore">
                                            </td>
                                            <td>{{ $payment['symbol'] }}</td>
                                            <td>{{ $payment['payment'] }} USD</td>
                                            <td>{{ $payment['date'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            {{ $payments->appends(
   ['user' => isset($user) ? $user : "none",
   'type' => isset($type) ? $type : "none",
   'crypto' => isset($crypto) ? $crypto : "none",
   'date' => isset($date) ? $date : '',
   ])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('authenticated_js')
    <script>

    </script>
@stop
