<form class="form" method="post" action="{{ route('permission.save') }}">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $permission['id'] ? $permission['id'] : 0 }}">
    <div class="form-body">
        <h4 class="form-section"><i class="icon-head"></i>{{ $permission ? __('messages.menu.editPermission') :  __('messages.menu.addPermission') }}</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="username">{{__('messages.menu.name')}}</label>
                    <input type="text" id="name" class="form-control" placeholder="{{__('messages.menu.name')}}" name="name"
                           value="{{ $permission['name'] }}">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover mb-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('messages.menu.menu')}}</th>
                            <th>{{__('messages.menu.hide')}}</th>
                            <th>{{__('messages.menu.read')}}</th>
                            <th>{{__('messages.menu.readAndWrite')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Helpers\Standard::routeList($permission) as $key => $route)
                            <tr>
                                <th scope="row">#</th>
                                <td>{{ $permission['id'] ? \App\Helpers\Standard::findRouteName($key) : \App\Helpers\Standard::findRouteName($route) }}</td>
                                <td>
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]"
                                               value="1" {{ $permission['id'] ? '' : 'checked' }} {{ (int)$route == 1 ? 'checked' : '' }}>
                                        {{__('messages.menu.hide')}}
                                    </label>
                                </td>
                                <td>
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]" value="2"
                                            {{ (int)$route == 2 ? 'checked' : '' }}>
                                        {{__('messages.menu.read')}}
                                    </label>
                                </td>
                                <td>
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="menu[{{ $permission['id'] ? $key : $route }}]" value="3"
                                            {{ (int)$route == 3 ? 'checked' : '' }}>
                                        {{__('messages.menu.full')}}
                                    </label>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions" style="text-align: end;">
        <button type="button" class="btn btn-warning mr-1 cancelModal">
            <i class="icon-cross2"></i>     {{__('messages.menu.cancelButton')}}
        </button>
        <button type="submit" class="btn btn-primary saveBtn">
            <i class="icon-check2"></i>     {{__('messages.menu.addUser')}}
        </button>
    </div>
</form>

<script>
    $('.cancelModal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#my-modal').modal('hide');
    });

    // $('.saveBtn').on('click', function (e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     alert('--');
    // });
</script>
