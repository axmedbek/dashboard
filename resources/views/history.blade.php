@extends('authenticated_layouts.app')

@section('authenticated_content')
    <div class="content-body" style="margin-top: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="display: flex">
                                <h4 class="card-title" style="margin-top: 5px;">{{__('messages.menu.history')}}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="permissions_table" class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{__('messages.menu.type')}}</th>
                                        <th>{{__('messages.menu.amount')}}</th>
                                        <th>{{__('messages.menu.cryptocurrency')}}</th>
                                        <th>{{__('messages.menu.icon')}}</th>
                                        <th>{{__('messages.menu.symbol')}}</th>
                                        <th>{{__('messages.menu.buyPrice')}}</th>
                                        <th>{{__('messages.menu.payment')}}</th>
                                        <th>{{__('messages.menu.yourProfit')}}</th>
                                        <th>{{__('messages.menu.date')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments as $key => $payment)
                                        <tr>
                                            <th scope="row">{{ $payments->firstItem() + $key }}</th>
                                            <td>
                                                @if ($payment['type'] === 'buy')
                                                    <span class="badge badge-success">Buy</span>
                                                @else
                                                    <span class="badge badge-danger">Sell</span>
                                                @endif
                                            </td>
                                            <td>{{ $payment['amount'] }}</td>
                                            <td>
                                                {{ $payment['cryptocurrency'] }}
                                            </td>
                                            <td>
                                                <img src='{{ url('/images/coins/'.strtolower($payment['symbol']).'.png') }}' alt="{{ $payment['cryptocurrency'] }} cryptostore">
                                            </td>
                                            <td>{{ $payment['symbol'] }}</td>
                                            <td>{{ $payment['price'] }} USD</td>
                                            <td>{{ $payment['payment'] }} USD</td>
                                            <td>
                                                @if ($payment['type'] === 'buy')
                                                    -
                                                @else
                                                    <span style="padding: 10px;font-size: 15px;"
                                                          class="badge badge-{{ $payment['payment'] - $payment['price'] > 0 ? 'success' : 'danger' }}">
                                                        {{ round(((double)$payment['payment'] - (double)$payment['price']),3) }} USD
                                                    </span>
                                                @endif
                                            </td>
                                            <td>{{ $payment['date'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            {{ $payments->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('authenticated_js')
    <script>

    </script>
@stop
