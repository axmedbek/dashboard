<div id="loading">
    <div>Loading...</div>
</div>

@include('admin.modals.modal')

<script src="{{ asset('js/global.js') }}"></script>

<script src="{{ asset('vendor/waves/waves.min.js') }}"></script>

<script src="{{ asset('vendor/validator/jquery.validate.js') }}"></script>
<script src="{{ asset('vendor/validator/validator-init.js') }}"></script>

<script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>

<!--  flot-chart js -->
<script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('vendor/apexchart/apexchart-init.js') }}"></script>


<!-- <script src="./js/dashboard.js"></script> -->

<script src="{{ asset('js/dashboard.js') }}"></script>
<script src="{{ asset('js/settings.js') }}"></script>
<script src="{{ asset('js/quixnav-init.js') }}"></script>
<script src="{{ asset('js/styleSwitcher.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script src="{{ asset('js/socket.io.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script src="{{ asset('vendor/sweetalert/sweetalert2.all.min.js') }}" type="text/javascript"></script>

<script>
    function getCryptoImageUrl(value) {
        return '{{ url('/images/coins/') }}/' + value.toLowerCase() + '.png';
    }

    function showLoading(status = false) {
        if (status) {
            $('#loading').show();
        }
        else {
            $('#loading').hide();
        }
    }

    function loadModal(route, data) {
        $.ajax({
            type: 'POST',
            url: route,
            data: data,
            headers: {
                'X-CSRF-Token': $('meta[name="CSRF_TOKEN"]').attr('content')
            },
            success: function (data) {
                $('#my-modal .modal-body').html('');
                $('#my-modal .modal-body').html(data);
            }
        });
        $('#my-modal').modal('show');
    }
</script>

@yield('js')

</body>

</html>
