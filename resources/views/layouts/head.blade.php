<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cryptostore – Buy &amp; Sell Bitcoin, Ethereum, and more with trust</title>
    <meta name="description"
          content="Cryptostore is a secure platform that makes it easy to buy, sell, and store cryptocurrency like Bitcoin, Ethereum, and more. Based in the USA, Cryptostore is available in over 30 countries worldwide."/>
    <meta name="keywords" content="cryptocurrency,bitcoin,ethereum"/>

    <meta property="og:title" content="Cryptostore – Buy &amp; Sell Bitcoin, Ethereum, and more with trust"/>
    <meta property="og:description"
          content="Cryptostore is a secure platform that makes it easy to buy, sell, and store cryptocurrency like Bitcoin, Ethereum, and more. Based in the USA, Cryptostore is available in over 30 countries worldwide."/>
    <meta property="og:image" content="https://logodix.com/logo/1647003.png"/>

    <meta name="google-site-verification" content=""/>
    <meta property="fb:app_id" content=""/>

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('vendor/nice-select/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl-carousel/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl-carousel/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/waves/waves.min.css') }}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/style.css?v=2') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/sweetalert/sweetalert2.min.css') }}">
    <meta name="CSRF_TOKEN" content="{{ csrf_token() }}">

    @yield('css')

    <style>
        .iti__country-list {
            z-index: 999 !important;
        }
    </style>
</head>
