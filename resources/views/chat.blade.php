@if (auth()->user()->isAdmin())
    @include('chat.admin')
@else
    @include('chat.customer')
@endif
