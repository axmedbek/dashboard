<?php

namespace App\Console\Commands;

use App\Cryptocurrency;
use App\Helpers\Standard;
use Illuminate\Console\Command;

class GetCryptoCurrenciesFromApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:crypto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get cryptocurrencies from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Standard::syncCryptoWithDatabase();
    }
}
