<?php


namespace App\Helpers;


use App\Cryptocurrency;
use App\CryptoValue;

class Standard
{
    const FULL_PERMISSION = 3;
    const READ_PERMISSION = 2;

    public static function locale()
    {
        return request()->cookie('my_locale',config('app.locale'));
    }

    public static function getCurrentEx(){
        if (app()->getLocale() === 'az') {
            return 'AZN';
        }
        else if (app()->getLocale() === 'en') {
            return 'USD';
        }
        else {
            return 'RUB';
        }
    }

    public static function routes()
    {
        return [
//            'dashboard',
            'admin.users',
            'permissions.admin',
            'admin.balance',
            'admin.history',
            'admin.chat'
        ];
    }

    public static function findRouteName($route){
        switch ($route) {
            case "admin.users" :
                return "Users Module";
            case "permissions.admin" :
                return "Permission Module";
            case "admin.history" :
                return "History Module";
            case "admin.balance" :
                return "Balance Module";
            case "admin.chat" :
                return "Chat Module";
        }
    }

    public static function routeList($permission)
    {
        $list = $permission['id'] ? (array)json_decode($permission['routes']) : Standard::routes();
        return $list;
    }


    public static function checkPermissionModule($module,$code = null){
        if (auth()->user()->is_superadmin) {
            return true;
        }
        else {
            if (auth()->user()->permission){
                foreach ((array)json_decode(auth()->user()->permission->routes) as $key => $route) {
                    if ($code) {
                        if($module == $key && (int)$route >= $code) return true;
                    }
                    else {
                        if($module == $key && (int)$route > 1) return true;
                    }
                }
            }
        }
        return false;
    }



    public static function getCurrencies($val){
        $url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';
        $parameters = [
            'start' => '1',
            'limit' => '100',
            'convert' => $val
        ];

        $headers = [
            'Accepts: application/json',
            'X-CMC_PRO_API_KEY: bd954a8e-9896-479c-84a1-68ac09de5ff2'
        ];
        $qs = http_build_query($parameters); // query string encode the parameters
        $request = "{$url}?{$qs}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,            // set the request URL
            CURLOPT_HTTPHEADER => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response = curl_exec($curl); // Send the request, save the response
        curl_close($curl); // Close request

        return $response;
    }

    public static function syncCryptoWithDatabase(){
        Cryptocurrency::query()->truncate();
        $response = self::getCurrencies('USD');
        foreach ((json_decode($response))->data as $item) {
            $crypto = new Cryptocurrency();
            $crypto->name = $item->name;
            $crypto->cmc_rank = $item->cmc_rank;
            $crypto->symbol = $item->symbol;
            $crypto->slug = $item->slug;
            $crypto->cid = $item->id;

            $crypto->price = $item->quote->USD->price;
            $crypto->volume_24h = $item->quote->USD->volume_24h;
            $crypto->percent_change_1h = $item->quote->USD->percent_change_1h;
            $crypto->percent_change_24h = $item->quote->USD->percent_change_24h;
            $crypto->percent_change_7d = $item->quote->USD->percent_change_7d;
            $crypto->market_cap = $item->quote->USD->market_cap;

            $crypto->save();
        }
    }

    public static function checkPermissionStatus($module,$code = null){
        if (auth()->user()->is_superadmin) {
            return true;
        }
        else {
            if (auth()->user()->permission){
                foreach ((array)json_decode(auth()->user()->permission->routes) as $key => $route) {
                    if ($code) {
                        if($module == $key && (int)$route >= $code) return true;
                    }
                    else {
                        if($module == $key && (int)$route > 2) return true;
                    }
                }
            }
        }
        return false;
    }

}
