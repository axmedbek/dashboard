<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCryptoCurrency extends Model
{
    use SoftDeletes;
}
