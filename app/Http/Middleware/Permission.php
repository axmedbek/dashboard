<?php

namespace App\Http\Middleware;

use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$module,$permission)
    {
        if (\App\Helpers\Standard::checkPermissionModule($module,$permission)) {
            return $next($request);
        }

        return abort(403, 'Unauthorized action.');
    }
}
