<?php

namespace App\Http\Middleware;

use Closure;

class BlockedUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->user_type === "user" && auth()->user()->is_blocked) {
            auth()->logout();
            return redirect()->route('login')->withErrors(['message' => 'Account was blocked. Contact with contact@crypstore.az']);
        }
        return $next($request);
    }
}
