<?php

namespace App\Http\Controllers\Admin;

use App\Balance;
use App\History;
use App\Http\Controllers\Controller;
use App\User;
use App\UserCryptoCurrency;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function index()
    {
        $payments = History::join('user_crypto_currencies', 'user_crypto_currencies.id', 'histories.user_crypto_currency_id')
            ->join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
            ->join('users', 'users.id', 'balances.user_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->select('users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency', 'histories.type',
                'cryptocurrencies.symbol', 'histories.created_at as date', 'histories.amount', 'histories.payment');

        if (request()->filled('crypto') && request()->get('crypto') != "none") {
            $payments->where('cryptocurrencies.id',request()->get('crypto'));
        }

        if (request()->filled('user') && request()->get('user') != "none") {
            $payments->where('users.id',request()->get('user'));
        }

        if (request()->filled('type') && request()->get('type') != "none") {
            $payments->where('histories.type',request()->get('type'));
        }

        if (request()->filled('date')) {
            $payments->whereDate('histories.created_at',request()->get('date'));
        }

        $payments = $payments->paginate(10);;

        $data = request()->all();
        $data['payments'] = $payments;

        return view('admin.history', $data);
    }

    public function addBalance($user_id)
    {
        return view('admin.balance_add', ['user_id' => $user_id]);
    }

    public function getTotalBalance($user_id)
    {
        $user = User::find($user_id);
        if (is_null($user)) {
            return abort(404, 'Resource not found.');
        }
        $balances = Balance::join('users', 'users.id', 'balances.user_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->where('users.id', $user_id)
            ->where('balances.amount','>',0)
            ->select('users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency',
                'cryptocurrencies.symbol', 'balances.created_at as date', 'balances.amount', 'balances.money as payment')
            ->paginate(6);
        return view('admin.balance_total', ['user' => $user, 'balances' => $balances]);
    }

    public function saveBalance(Request $request)
    {
        $validator = validator($request->all(), [
            'amount' => 'required|integer',
            'cryptocurrency' => 'required|integer|exists:cryptocurrencies,id',
            'user_id' => 'required|integer|exists:users,id',
            'payment' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $balance = Balance::where('cryptocurrency_id', $request->get('cryptocurrency'))
                ->where('user_id', $request->get('user_id'))
                ->first();

            if (is_null($balance)) {
                $balance = new Balance();
                $balance->amount = $request->get('amount');
                $balance->money = $request->get('payment');
                $balance->cryptocurrency_id = $request->get('cryptocurrency');
                $balance->user_id = $request->get('user_id');
                $balance->operation_user_id = auth()->user()->id;
                $balance->save();
            } else {
                $balance->amount = $balance->amount + $request->get('amount');
                $balance->money = $balance->money + $request->get('payment');
                $balance->cryptocurrency_id = $request->get('cryptocurrency');
                $balance->user_id = $request->get('user_id');
                $balance->operation_user_id = auth()->user()->id;
                $balance->save();
            }


            $userCrypto = UserCryptoCurrency::where('balance_id', $balance->id)->where('buy_cost', $request->get('payment'))->first();
            if (is_null($userCrypto)) {
                $userCrypto = new UserCryptoCurrency();
                $userCrypto->balance_id = $balance->id;
                $userCrypto->amount = $request->get('amount');
                $userCrypto->buy_cost = $request->get('payment');
            } else {
                $userCrypto->amount = $userCrypto->amount + $request->get('amount');
            }
            $userCrypto->save();

            $history = new History();
            $history->user_crypto_currency_id = $userCrypto->id;
            $history->type = 'buy';
            $history->amount = $request->get('amount');
            $history->payment = $request->get('payment');
            $history->save();

            return redirect()->route('admin.users.balance', $request->get('user_id'));
        }
    }

    public function removeBalance($id)
    {
        $userCrypto = UserCryptoCurrency::join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->where('user_crypto_currencies.id', $id)
            ->first(['user_crypto_currencies.id', 'user_crypto_currencies.buy_cost', 'user_crypto_currencies.amount', 'cryptocurrencies.name', 'cryptocurrencies.symbol']);

        if (is_null($userCrypto)) {
            return abort(404, 'Resource not found.');
        } else {
            return view('admin.balance_remove', ['crypto' => $userCrypto]);
        }
    }

    public function removeBalanceProcess(Request $request)
    {
        $validator = validator($request->all(), [
            'amount' => 'required|integer',
            'crypto_id' => 'required|integer|exists:user_crypto_currencies,id',
            'payment' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $userCrypto = UserCryptoCurrency::find($request->get('crypto_id'));
            $balance = Balance::join('user_crypto_currencies','user_crypto_currencies.balance_id','balances.id')
                ->where('user_crypto_currencies.id',$request->get('crypto_id'))
                ->first(['balances.*']);

            if ((int)$userCrypto->amount < $request->get('amount')) {
                return redirect()->back()->withErrors(['message' => 'There is not enough crypto currencies for sell'])->withInput();
            } else {
                $userCrypto->amount = $userCrypto->amount - $request->get('amount');
                $userCrypto->save();

                if (!is_null($balance)) {
                    $balance->amount = $balance->amount - $request->get('amount');
                    $balance->save();
                }

                $history = new History();
                $history->user_crypto_currency_id = $userCrypto->id;
                $history->type = 'sell';
                $history->amount = $request->get('amount');
                $history->payment = $request->get('payment');
                $history->save();

                return redirect()->route('admin.history');
            }
        }
    }
}
