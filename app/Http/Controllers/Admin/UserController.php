<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PermissionGroup;
use App\User;
use App\UserCryptoCurrency;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {

        $users = User::where('is_superadmin', '!=', 1)->where('user_type', '=', 'user')->paginate(10);
        $systemUsers = User::where('is_superadmin', '!=', 1, 'AND')->where('user_type', '=', 'admin')->paginate(10);
        return view('admin.users.users', compact('users', 'systemUsers'));
    }

    public function userStatus($user_id)
    {
        $validator = validator(['user_id' => $user_id], [
            'user_id' => 'required|integer|exists:users,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $user = User::find($user_id);
            if ($user->is_blocked == true) {
                $user->is_blocked = false;
            } else {
                $user->is_blocked = true;
            }
            $user->save();
            return response()->json(['status' => true, 'data' => $user->is_blocked]);
        }
    }

    public function userDeleted($user_id)
    {
        $validator = validator(['user_id' => $user_id], [
            'user_id' => 'required|integer|exists:users,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        } else {
            $user = User::find($user_id);
            $user->delete();
            return response()->json(['status' => true, 'data' => $user->delete_at]);
        }

    }

    public function infoPage($user_id)
    {
//        return redirect()->back()->withErrors($errors);
        $user = User::find($user_id);
        if ($user->user_type == 'admin') {
            $permissions = PermissionGroup::all();
            return view('admin.users.info', ['user' => $user, 'permissions' => $permissions]);
        } else {
            return view('admin.users.info', ['user' => $user]);
        }
    }

    public function editPage($user_id)
    {
        $user = User::find($user_id);
        if ($user->user_type == 'admin') {
            $permissions = PermissionGroup::all();
            return view('admin.users.edit', ['user' => $user, 'permissions' => $permissions]);
        } else {
            return view('admin.users.edit', ['user' => $user]);
        }
    }

    public function userAdd()
    {
        $permissions = PermissionGroup::all();
        return view('admin.users.add', ['permissions' => $permissions]);
    }

    public function updateUser(Request $request, $user_id)
    {
        $validator = validator($request->all(),
            [
                'permission' => 'integer|exists:permission_groups,id',
                'user_id' => 'integer|exists:users,id',
                'name' => 'required|string',
                'surname' => 'required|string',
                'phone' => 'required|string',
                'email' => 'required|string',
                'password' => 'nullable|string|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $user = User::find($user_id);
            $user->name = $request->get('name');
            $user->surname = $request->get('surname');
            $user->phone = $request->get('phone');
            $user->email = $request->get('email');
            if($request->filled('permission')) {
                $user->permission_group_id = $request->get('permission');
            }

            if($request->filled('password')) {
                $user->password = bcrypt($request->get('password'));
            }
            $user->save();

            return redirect()->route('admin.users');
        }
    }

    public function newPerson(Request $request)
    {
        $validator = validator($request->all(),
            [
                'permission' => 'required|integer|exists:permission_groups,id',
                'name' => 'required|string',
                'surname' => 'required|string',
                'phone' => 'required|string|unique:users,phone',
                'email' => 'required|string|email|unique:users,email',
                'password' => 'required|string|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $user = new User();
            $user->name = $request->get('name');
            $user->surname = $request->get('surname');
            $user->phone = $request->get('phone');
            $user->email = $request->get('email');
            $user->user_type = 'admin';
            $user->email_verified_at = Carbon::now();
            $user->permission_group_id = $request->get('permission');
            $user->password = bcrypt($request->get('password'));
            $user->save();

            return redirect()->route('admin.users');
        }
    }


    public function getUserBalance($user_id)
    {
        $user = User::find($user_id);
        if (is_null($user)) {
            return abort(404, 'Resource not found.');
        }
        $balance = UserCryptoCurrency::join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
            ->join('users', 'users.id', 'balances.user_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->select('user_crypto_currencies.id','users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency',
                'cryptocurrencies.symbol', 'user_crypto_currencies.created_at as date',
                'user_crypto_currencies.amount', 'user_crypto_currencies.buy_cost')
            ->where('user_crypto_currencies.amount','>',0)
            ->where('users.id', $user_id)->paginate(10);
        return view('admin.balance',['balances' => $balance,'user' => $user]);
    }
}
