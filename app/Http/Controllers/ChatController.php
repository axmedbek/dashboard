<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        $users = [];
        $messages = [];
        if (auth()->user()->isAdmin()) {
            $users = Chat::join('users', 'users.id', '=', 'chats.user_id')
                ->where('users.id', '!=', 1)
                ->groupBy(['users.id', 'users.name', 'users.surname', 'users.email'])
                ->get(['users.id', 'users.name', 'users.surname', 'users.email']);
            $usersB = Chat::join('users', 'users.id', '=', 'chats.sender_id')
                ->where('users.id', '!=', 1)
                ->groupBy(['users.id', 'users.name', 'users.surname', 'users.email'])
                ->get(['users.id', 'users.name', 'users.surname', 'users.email']);

            $users = $users->merge($usersB);
        } else {
            $messages = Chat::where('sender_id', auth()->user()->id)
                ->orWhere('user_id', auth()->user()->id)
                ->get();
        }
        return view('chat', [
            'users' => $users,
            'messages' => $messages
        ]);
    }

    public function getChatBox()
    {
        $messages = Chat::where('sender_id', auth()->user()->id)
            ->where('user_id', request()->get('user_id'))
            ->orWhere('sender_id', request()->get('user_id'))
            ->where('user_id', auth()->user()->id)
            ->get();

        $user = User::find(request()->get('user_id'));
        return view('chat.admin_chatbox', ['messages' => $messages, 'user' => $user]);
    }

    public function getChatUserInfo()
    {
        $authType = request('authType');

        if (!request()->filled('authType') || request('authType') == "") {
            return response()->json(['status' => false, 'message' => 'Parameters are incorrect']);
        }

        $user = auth($authType)->user();

        if ($user == null) {
            return response()->json(['status' => 'error', 'message' => 'Not found user']);
        }

        $user['cryptostore_session'] = $_COOKIE['cryptostore_session'];

        return response()->json(['status' => true, 'user' => $user]);

    }

    public function saveChatMessage(Request $request)
    {
        $validator = validator($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'type' => 'required|string|in:admin,user',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false]);
        } else {

            if ($request->get('type') == 'admin') {
                $admins = User::where('user_type','admin')->get(['id']);
                foreach ($admins as $admin) {
                    $chat = new Chat();
                    $chat->sender_id = auth()->user()->id;
                    $chat->user_id = $admin['id'];
                    $chat->message = $request->get('message');
                    $chat->save();
                }
            }
            else {
                $chat = new Chat();
                $chat->sender_id = auth()->user()->id;
                $chat->user_id = $request->get('user_id');
                $chat->message = $request->get('message');
                $chat->save();
            }

            return response()->json(['status' => true, 'data' => $chat]);
        }
    }

    public function makeUserOnline(){
        $user_id = request('user_id');
        if ($user_id == null || $user_id <= 0) {
            return response()->json(['status' => 'error', 'message' => 'Parameters is incorrect']);
        }
        $user = User::find($user_id);
        if ($user == null) {
            return response()->json(['status' => 'error', 'message' => 'There is not a user with this user_id']);
        }

        if (request('status') > 0) {
            $user->status = true;
        }
        else {
            $user->status = false;
            $user->last_active_date = Carbon::now();
        }

        $user->save();

        return response()->json(['status' => true]);

    }
}
