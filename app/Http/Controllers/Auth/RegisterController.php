<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserImage;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function register()
    {
        $validator = validator(request()->all(), [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|string|email|unique:users,email',
            'phone' => 'required|string|unique:users,phone',
            'password' => 'required|string|min:6|confirmed',
            'images' => 'required|array|max:2'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            DB::beginTransaction();
            try {

                $user = new User();
                $user->name = request('name');
                $user->surname = request('surname');
                $user->email = request('email');
                $user->phone = request('phone');
                $user->password = bcrypt(request('password'));
                $user->save();

                foreach (request('images') as $file) {
                    $imageName = time().rand(1000,9999).'.'.$file->extension();
                    $file->move(public_path('images/id-images'), $imageName);
                    $userImage = new UserImage();
                    $userImage->path = $imageName;
                    $userImage->user_id = $user->id;
                    $userImage->save();
                }
                DB::commit();

                auth()->attempt(['email' => request('email'),'password' => request('password')]);

                $user->sendEmailVerificationNotification();

                return redirect()->route('verification.notice');
            } catch (\Throwable $t) {
                DB::rollBack();
//                'Registration problem - #186'
                return redirect()->back()->withErrors(['message' => 'Operation problem.Contact with administrator']);
            }

        }
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
