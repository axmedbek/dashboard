<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function sendResetLink(Request $request)
    {
        $validator = validator(request()->all(), [
            'email' => 'required|string|email|exists:users,email'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $user = User::where('email', $request->get('email'))->first();
            $token = Str::random(60);

            DB::table('password_resets')->insert([
                'email' => $request->get('email'),
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $user->sendPasswordResetNotification($token);

            Session::put('reset_email', $request->get('email'));

            return redirect()->route('password.reset.sending');
        }
    }

    public function showResetForm(Request $request)
    {
        $userTokenCheck = DB::table('password_resets')->where('email', $request->get('email'))
            ->where('token', $request->get('token'))
            ->first();

        if (is_null($userTokenCheck)) {
            throw new AuthorizationException;
        } else {
            return view('auth.passwords.reset-form');
        }
    }


    public function reset(Request $request)
    {
        $userTokenCheck = DB::table('password_resets')->where('email', $request->get('email'))
            ->where('token', $request->get('token'))
            ->first();

        if (is_null($userTokenCheck)) {
            throw new AuthorizationException;
        } else {
            if ($request->get('password') !== $request->get('password_confirmation')) {
                return redirect()->back()->withErrors(['message' => 'Password is not equal with confirmed password']);
            } else {
                $user = User::where('email', $request->get('email'))->first();
                if (is_null($user)) {
                    throw new AuthorizationException;
                } else {
                    $user->password = bcrypt($request->get('password'));
                    $user->save();
                    if (auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
                        return redirect()->route('home');
                    } else {
                        throw new AuthorizationException;
                    }
                }
            }
        }
    }

}
