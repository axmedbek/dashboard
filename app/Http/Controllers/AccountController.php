<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        $payments = History::join('user_crypto_currencies', 'user_crypto_currencies.id', 'histories.user_crypto_currency_id')
            ->join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
            ->join('users', 'users.id', 'balances.user_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->select('users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency', 'histories.type',
                'cryptocurrencies.symbol','user_crypto_currencies.buy_cost as price', 'histories.created_at as date', 'histories.amount', 'histories.payment')
            ->where('users.id',auth()->user()->id)
            ->paginate(10);
        return view('history',['payments' => $payments]);
    }
}
