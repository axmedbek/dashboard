<?php

namespace App\Http\Controllers;

use App\User;
use App\UserImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function index()
    {
        return view('setting');
    }

    public function changePassword(Request $request)
    {
        $validator = validator($request->all(), [
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            auth()->user()->password = bcrypt($request->get('password'));
            auth()->user()->save();
            return redirect()->back()->with(['message' => 'Password was changed successfully']);
        }
    }

    public function userUpdateSetting()
    {
        $validator = validator(request()->all(), [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|string|email|unique:users,email,' . auth()->user()->id . ',id',
            'phone' => 'required|string|unique:users,phone,' . auth()->user()->id . ',id',
            'images' => 'nullable|array|max:2'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            DB::beginTransaction();
            try {

                $user = auth()->user();
                $user->name = request('name');
                $user->surname = request('surname');
                $user->email = request('email');
                $user->phone = request('phone');
                $user->save();

                $imagesCount = auth()->user()->images() ? auth()->user()->images()->count() : 0;
                $needImageCount = 2 - (int)$imagesCount;
                $uploadImageCount = count(request('images',[]));

                if ($imagesCount > 1 && count(request('images',[])) > 0) {
                    return redirect()->back()->withErrors(['message' => 'There are two id card.Please delete any and update it']);
                }

                if ($uploadImageCount === 2 && $needImageCount === 1) {
                    return redirect()->back()->withErrors(['message' => 'You must upload 1 image.There is 1 image already.Delete it and upload 2 images']);
                }

                foreach (request('images',[]) as $file) {
                    $imageName = time() . rand(1000, 9999) . '.' . $file->extension();
                    $file->move(public_path('images/id-images'), $imageName);
                    $userImage = new UserImage();
                    $userImage->path = $imageName;
                    $userImage->user_id = $user->id;
                    $userImage->save();
                }

                DB::commit();


                return redirect()->back()->with(['message' => 'User information was changed successfully']);
            } catch (\Throwable $t) {
                DB::rollBack();
//                'Registration problem - #186'
                return redirect()->back()->withErrors(['message' => 'Operation problem.Contact with administrator']);
            }
        }
    }

    public function userImageDelete(Request $request)
    {
        $validator = validator(request()->all(), [
            'id' => 'required|integer|exists:user_images,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false]);
        } else {
            $userImage = UserImage::find($request->get('id'));
            @unlink(public_path('images/id-images') . "/" . $userImage['path']);
            $userImage->delete();
            return response()->json(['status' => true]);
        }
    }
}
