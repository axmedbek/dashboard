<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Cryptocurrency;
use App\Helpers\Standard;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function landingPage()
    {
        return redirect('https://cryptostore.grinduck.com/');
    }

    public function changeLang($locale)
    {
        $cookie = cookie('my_locale',$locale,45000);
        return redirect()->back()->cookie($cookie);
    }

    public function getCryptoCurrencies(Request $request){
        $response = Cryptocurrency::take($request->get('limit',100))->get();
        return response()->json(['status' => true,'data' => $response]);
    }
}
