<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Helpers\Standard;
use App\UserCryptoCurrency;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function index()
    {
        $balances = UserCryptoCurrency::join('balances', 'balances.id', 'user_crypto_currencies.balance_id')
            ->join('users', 'users.id', 'balances.user_id')
            ->join('cryptocurrencies', 'cryptocurrencies.id', 'balances.cryptocurrency_id')
            ->select('user_crypto_currencies.id','users.name', 'users.surname', 'cryptocurrencies.name as cryptocurrency',
                'cryptocurrencies.symbol','cryptocurrencies.id as crypto_id', 'user_crypto_currencies.created_at as date',
                'user_crypto_currencies.amount', 'user_crypto_currencies.buy_cost','cryptocurrencies.price')
            ->where('user_crypto_currencies.amount','>',0)
            ->where('users.id', auth()->user()->id)->paginate(10);


        return view('balance',['balances' => $balances]);
    }
}
