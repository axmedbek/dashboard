const PORT = 2222;
const io = require('socket.io')(PORT),
    _ = require('underscore'),
    md5 = require('md5'),
    http = require('http'),
    path = require('path'),
    querystring = require('querystring');
require('dotenv').config({path: path.join(__dirname, "/.env")});

const API_HOST = process.env.API_HOST;
const API_PORT = process.env.API_PORT;
const API_PATH_PREFIX = process.env.API_PATH_PREFIX;
var users = Object.create(null);

io.use(function (socket, next) {
    let handshakeQuery = socket.handshake.query;
    var postedUserInfoDataQueryString = querystring.stringify({'authType': handshakeQuery.authType,'_token' : handshakeQuery._token});
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/get/chat/info',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedUserInfoDataQueryString),
            'Cookie': `cryptostore_session=${handshakeQuery.token}`

        }
    };

    let req = http.request(options, function (response) {
        let data = '';
        response.on('error', function () {
            console.log(" io use 1 was happened unexpected error");
            return next(new Error("not authorized"));
        });

        response.on('data', function (chunk) {
            data += chunk.toString();
        });

        response.on('end', function () {
            try {
                var userData = JSON.parse(data);
                if (!userData.status) {
                    console.log('error');
                }
            } catch (e) {
                console.log(e);
                return next(new Error("not authorized"));
            }

            let connectedUserId = userData.user.id;

            if (_.isUndefined(users[connectedUserId])) {
                users[connectedUserId] = userData.user;
                users[connectedUserId].socketIds = [];
            }

            users[connectedUserId].socketIds.push(socket.id);
            next();
        });
    });
    req.write(postedUserInfoDataQueryString);
    req.on('error', function (e) {
        console.log(" io use 2 was happened unexpected error");
        console.log(e);
        return next(new Error("not authorized"));
    });

    req.end();

});

io.on('connection', function (socket) {
    let userInfo = getUserInformationBySocketId(socket.id);
    if (!_.isNull(users)) {

        makeOfflineOrOnlineUser({
            'user_id': userInfo.id,
            'cryptostore': userInfo.cryptostore,
            'status' : 1
        });

        _.each(users, function (user) {
            user.socketIds.forEach(function (socketId) {
                io.to(socketId).emit('user online', {
                    'user_id': userInfo.id,
                });
            });
        });
    }
    socket.on('new message', function (response) {
        saveMesssage({
            'user_id': response.user_id,
            'message': response.message,
            '_token': response._token,
            'cryptostore_session': response.cryptostore_session,
        });

        if (_.isUndefined(users[response.user_id])) {
            return;
        }

        users[response.user_id].socketIds.forEach(function (socketId) {
            io.to(socketId).emit('new message', {
                'user_id': response.sender_id,
                'name': users[response.sender_id].name,
                'surname': users[response.sender_id].surname,
                'email': users[response.sender_id].email,
                'connection_id': response.sender_id,
                'content': response.message,
            });
        });
    });

    socket.on('disconnect', function () {
        if (_.isNull(userInfo)) {
            return;
        }
        // removing socket from user
        userInfo.socketIds = _.without(userInfo.socketIds, socket.id);

        if (userInfo.socketIds.length === 0) {
            makeOfflineOrOnlineUser({
                'user_id': userInfo.id,
                'cryptostore_session': userInfo.cryptostore_session,
                'status' : 0
            });

            _.each(users, function (user) {
                user.socketIds.forEach(function (socketId) {
                    io.to(socketId).emit('user offline', {
                        'user_id': userInfo.id,
                        'last_active_date': userInfo.last_active_date
                    });
                });
            });

            delete users[userInfo.id];
        }
    });
});

function saveMesssage(postedData) {
    var postedDataQueryString = querystring.stringify(postedData);
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/save/chat/message',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedDataQueryString),
            'Cookie': 'cryptostore_session=' + postedData.cryptostore_session

        }
    };
    let req = http.request(options, function (response) {
        var data = '';
        response.on('data', function (chunk) {
            data += chunk.toString();
        });

        response.on('end', function () {
            if (data.status === "error") {
                throw new Error("validation error");
            } else {
                console.log(data);
                data = JSON.parse(data);
                if (data.status === "success") {
                    if (_.isUndefined(users[data.chat.user_id])) {
                        return;
                    }
                    users[data.chat.user_id].socketIds.forEach(function (socketId) {
                        io.to(socketId).emit('new notify', {
                            'id': data.chat.id,
                            'message': data.chat.message,
                        });
                    });
                }
            }
        });
    });
    req.write(postedDataQueryString);
    req.on('error', function (e) {
        console.log(e);
        // console.log('There is an error with saving message');
    });
    req.end();
}

function makeOfflineOrOnlineUser(postedData) {
    var postedOfflineDataQueryString = querystring.stringify(postedData);
    let options = {
        host: API_HOST,
        port: API_PORT,
        path: API_PATH_PREFIX + '/chat/user/online',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postedOfflineDataQueryString),
            'Cookie': 'cryptostore_session=' + postedData.cryptostore_session

        }
    };

    let req = http.request(options, function (response) {
        let data = '';
        response.on('data', function (chunk) {
            data += chunk.toString();
        });
        response.on('end', function () {

        });
    });

    req.write(postedOfflineDataQueryString);
    req.on('error', function () {

    });
    req.end();
}

function getUserByGroupId(group_id, sender_id) {
    var filteredGroupUsers = {};
    _.each(users, function (user, key) {
        _.each(user.groups, function (group) {
            if (group.group_id == group_id && user.id != sender_id) {
                filteredGroupUsers[key] = user;
            }
        });
    });
    return filteredGroupUsers;
}

function getUserInformationBySocketId(socketId) {
    for (let userId in users) {
        let userSockets = users[userId].socketIds;
        for (var i = 0, len = userSockets.length; i < len; ++i) {
            if (socketId === userSockets[i]) {
                return users[userId];
            }
        }
    }
    return null;
}

console.log(API_HOST + " " + API_PORT + " is listening");
