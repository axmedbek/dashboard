<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = "Crypto";
        $user->surname = "Admin";
        $user->phone = "994999999999";
        $user->email = "admin@mail.ru";
        $user->email_verified_at = \Carbon\Carbon::now();
        $user->user_type = "admin";
        $user->is_superadmin = true;
        $user->password = bcrypt("crypto!@#$%^&*()");
        $user->save();

        for ($i = 1 ; $i < 5; $i++) {
            $user = new \App\User();
            $user->name = "User".$i;
            $user->surname = "Test".$i;
            $user->phone = "99499999999".$i;
            $user->email = "usermail".$i."@mail.ru";
            $user->email_verified_at = \Carbon\Carbon::now();
            $user->user_type = "user";
            $user->password = bcrypt("123456");
            $user->save();
        }
    }
}
