<?php

use App\Cryptocurrency;
use Illuminate\Database\Seeder;

class CryptocurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \App\Helpers\Standard::syncCryptoWithDatabase();
    }
}
