<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCryptocurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cryptocurrencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('symbol');
            $table->string('slug');
            $table->integer('cmc_rank');
            $table->unsignedBigInteger('cid');

            $table->decimal('price',20,4);
            $table->decimal('volume_24h',20,4);
            $table->decimal('percent_change_1h',9,6);
            $table->decimal('percent_change_24h',9,6);
            $table->decimal('percent_change_7d',9,6);
            $table->decimal('market_cap',20,4);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cryptocurrencies');
    }
}
