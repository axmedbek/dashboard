<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->string('phone');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('temp_email')->unique()->nullable();
            $table->string('password');
            $table->enum('user_type', ['user', 'admin'])->default("user");
            $table->dateTime('last_active_date')->nullable();
            $table->boolean('is_online')->default(false);
            $table->boolean('is_blocked')->default(false);
            $table->boolean('is_superadmin')->default(false);
            $table->unsignedBigInteger('permission_group_id')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
