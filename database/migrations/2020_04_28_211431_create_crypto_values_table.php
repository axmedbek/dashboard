<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCryptoValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cryptocurrency_id');
            $table->string('lang');
            $table->decimal('price',20,4);
            $table->decimal('volume_24h',20,4);
            $table->integer('percent_change_1h');
            $table->integer('percent_change_24h');
            $table->integer('percent_change_7d');
            $table->decimal('market_cap',20,4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_values');
    }
}
