<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application');

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::group(['middleware' => ['guest:web']], function(){

Route::get('/get/crypto/list', 'HomeController@getCryptoCurrencies')->name('crypto.list');
Route::get('/landingPage', 'HomeController@landingPage')->name('landing.page');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::post('/register', 'Auth\RegisterController@register');


// password reset links
Route::get('/reset/request/form', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::get('/reset/request/sending', 'Auth\ForgotPasswordController@resetRequestSending')->name('password.reset.sending');
Route::post('/reset/request/send', 'Auth\ResetPasswordController@sendResetLink')->name('password.send.link');
Route::post('/reset/password', 'Auth\ResetPasswordController@reset')->name('password.reset');
Route::get('/reset/password', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.form');
//});


Route::group(['middleware' => ['blocked', 'auth:web', 'lang']], function () {
    Route::get('change/lang/{locale}', 'HomeController@changeLang')->where('locale', '(az)|(en)|(ru)')->name('change.lang');
//    Route::get('mail/verify-page', 'Auth\VerificationController@show')->name('auth.mail.page');

    Route::group(['middleware' => 'mail'], function () {
        Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
        Route::get('email/logout', 'Auth\VerificationController@logoutFromVerification')->name('verification.logout');
        Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
        Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
    });

    Route::group(['middleware' => 'verified'], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('chat', 'ChatController@index')->name('chat');

        Route::get('setting', 'SettingController@index')->name('setting');
        Route::post('change/password', 'SettingController@changePassword')->name('user.change.password');
        Route::post('user/update', 'SettingController@userUpdateSetting')->name('setting.user.update');
        Route::post('user/image/delete', 'SettingController@userImageDelete')->name('setting.user.delete.image');

        Route::get('history', 'AccountController@index')->name('history');
        Route::get('balance', 'BalanceController@index')->name('balance');
        Route::get('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('exchange', 'ExchangeController@index')->name('exchange');

        Route::post('/get/chat/info', 'ChatController@getChatUserInfo')->name('chat.user.info');
        Route::post('/chat/user/online', 'ChatController@makeUserOnline')->name('chat.user.online');
        Route::post('/save/chat/message', 'ChatController@saveChatMessage')->name('chat.message.save');
        Route::get('/get/chatbox', 'ChatController@getChatBox')->name('chat.box');
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
        // to get users
        Route::get('/users', 'UserController@index')->name('admin.users')->middleware('permission:admin.users,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::put('/user/status/{user_id}', 'UserController@userStatus')->name('admin.users.block')->middleware('permission:admin.users,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::delete('/users/{user_id}', 'UserController@userDeleted')->name('admin.users.delete')->middleware('permission:admin.users,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::get('/users/edit/{user_id}', 'UserController@editPage')->name('admin.users.edit')->middleware('permission:admin.users,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::post('/users/edit/{user_id}', 'UserController@updateUser')->name('admin.users.updateInfo')->middleware('permission:admin.users,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::get('/users/info/{user_id}', 'UserController@infoPage')->name('admin.users.info')->middleware('permission:admin.users,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::get('/user/add', 'UserController@userAdd')->name('admin.users.add')->middleware('permission:admin.users,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::post('/user/add', 'UserController@newPerson')->name('admin.users.save')->middleware('permission:admin.users,'.\App\Helpers\Standard::FULL_PERMISSION);

        Route::get('/user/balance/{user_id}', 'UserController@getUserBalance')->name('admin.users.balance')->middleware('permission:admin.balance,'.\App\Helpers\Standard::READ_PERMISSION);

//        Route::get('/user/add', 'UserController@userAdd')->name('admin.users.add')->middleware('permission:admin.users');
//        Route::post('/user/add', 'UserController@newPerson')->name('admin.users.save')->middleware('permission:admin.users');

        Route::get('/permissions', 'PermissionController@index')->name('permissions.admin')->middleware('permission:permissions.admin,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::post('/permission/add-edit/{id}', 'PermissionController@permissionAddEdit')->name('permissions.add.edit')->middleware('permission:permissions.admin,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::post('/permission/save', 'PermissionController@permissionSave')->name('permission.save')->middleware('permission:permissions.admin,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::post('/permission/delete', 'PermissionController@permissionDelete')->name('permission.delete.admin')->middleware('permission:permissions.admin,'.\App\Helpers\Standard::FULL_PERMISSION);

        Route::get('/history', 'BalanceController@index')->name('admin.history')->middleware('permission:admin.history,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::get('/balance/add/{user_id}', 'BalanceController@addBalance')->name('admin.balance.add')->middleware('permission:admin.balance,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::get('/balance/remove/{id}', 'BalanceController@removeBalance')->name('admin.balance.remove')->middleware('permission:admin.balance,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::post('/balance/remove', 'BalanceController@removeBalanceProcess')->name('admin.balance.remove.process')->middleware('permission:admin.balance,'.\App\Helpers\Standard::FULL_PERMISSION);
        Route::get('/balance/total/{user_id}', 'BalanceController@getTotalBalance')->name('admin.balance.total')->middleware('permission:admin.balance,'.\App\Helpers\Standard::READ_PERMISSION);
        Route::post('/balance/add', 'BalanceController@saveBalance')->name('admin.balance.save')->middleware('permission:admin.balance,'.\App\Helpers\Standard::FULL_PERMISSION);
    });

    Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
        Route::get('chat', 'ChatController@index')->name('admin.chat')->middleware('permission:admin.chat,'.\App\Helpers\Standard::READ_PERMISSION);
    });
});
